% Estimator Comparison
% compares logged data between estimators

clc
clear
close all


% createVideo='true';
createVideo='false';

%% Read logged data
addpath Underwater_Vehicle
addpath Surface_Vessel
addpath Multirotor_Helicopter
addpath Helper
addpath Logs

tHist=load('Logs/tHist.mat');
tHist=tHist.tHist;
dt=tHist(2)-tHist(1);

truth_SVmuHist=load('Logs/xHist_SV.mat');
truth_MHmuHist=load('Logs/xHist_MH.mat');
truth_UVmuHist=load('Logs/xHist_UV.mat');

indiv_SVmuHist=load('Logs/indiv_SVmuHist.mat');
indiv_MHmuHist=load('Logs/indiv_MHmuHist.mat');
indiv_UVmuHist=load('Logs/indiv_UVmuHist.mat');

mono_SVmuHist=load('Logs/mono_SVmuHist.mat');
mono_MHmuHist=load('Logs/mono_MHmuHist.mat');
mono_UVmuHist=load('Logs/mono_UVmuHist.mat');

joint_SVmuHist=load('Logs/joint_SVmuHist.mat');
joint_MHmuHist=load('Logs/joint_MHmuHist.mat');
joint_UVmuHist=load('Logs/joint_UVmuHist.mat');

%% Normalized RMSE report
% angle estimation error has a multiplyer of x100 as an attempt to
% "normalize" value between [m] and [rad].
%
% previous attempt at normalizing with RMSE/(max()-min()) didn't really
% worked out as the result will be skewed towards vehicle that patrols around the starting location, or travelled too far away.
% 

% individual estimation
index=1:3;
indivPosError_SV=truth_SVmuHist.xHist_SV(index,:)-indiv_SVmuHist.SVmuHist(index,:);
indivPosError_MH=truth_MHmuHist.xHist_MH(index,:)-indiv_MHmuHist.MHmuHist(index,:);
indivPosError_UV=truth_UVmuHist.xHist_UV(index,:)-indiv_UVmuHist.UVmuHist(index,:);
indivPosError=[indivPosError_SV,indivPosError_MH,indivPosError_UV];

index=4:6;
indivRotError_SV=truth_SVmuHist.xHist_SV(index,:)-indiv_SVmuHist.SVmuHist(index,:);
indivRotError_MH=truth_MHmuHist.xHist_MH(index,:)-indiv_MHmuHist.MHmuHist(index,:);
indivRotError_UV=truth_UVmuHist.xHist_UV(index,:)-indiv_UVmuHist.UVmuHist(index,:);
indivRotError=[indivRotError_SV,indivRotError_MH,indivRotError_UV];

index=7:9;
indivAccelError_SV=truth_SVmuHist.xHist_SV(index,:)-indiv_SVmuHist.SVmuHist(index,:);
indivAccelError_MH=truth_MHmuHist.xHist_MH(index,:)-indiv_MHmuHist.MHmuHist(index,:);
indivAccelError_UV=truth_UVmuHist.xHist_UV(index,:)-indiv_UVmuHist.UVmuHist(index,:);
indivAccelError=[indivAccelError_SV,indivAccelError_MH,indivAccelError_UV];

index=10:12;
indivAngAccelError_SV=truth_SVmuHist.xHist_SV(index,:)-indiv_SVmuHist.SVmuHist(index,:);
indivAngAccelError_MH=truth_MHmuHist.xHist_MH(index,:)-indiv_MHmuHist.MHmuHist(index,:);
indivAngAccelError_UV=truth_UVmuHist.xHist_UV(index,:)-indiv_UVmuHist.UVmuHist(index,:);
indivAngAccelError=[indivAngAccelError_SV,indivAngAccelError_MH,indivAngAccelError_UV];

IndivNRMSE=sqrt(sum(sum(indivPosError.^2+100*indivRotError.^2+indivAccelError.^2+100*indivAngAccelError.^2))/(length(indivAngAccelError)*12));

% mono estimation
index=1:3;
monoPosError_SV=truth_SVmuHist.xHist_SV(index,:)-mono_SVmuHist.SVmuHist(index,:);
monoPosError_MH=truth_MHmuHist.xHist_MH(index,:)-mono_MHmuHist.MHmuHist(index,:);
monoPosError_UV=truth_UVmuHist.xHist_UV(index,:)-mono_UVmuHist.UVmuHist(index,:);
monoPosError=[monoPosError_SV,monoPosError_MH,monoPosError_UV];

index=4:6;
monoRotError_SV=truth_SVmuHist.xHist_SV(index,:)-mono_SVmuHist.SVmuHist(index,:);
monoRotError_MH=truth_MHmuHist.xHist_MH(index,:)-mono_MHmuHist.MHmuHist(index,:);
monoRotError_UV=truth_UVmuHist.xHist_UV(index,:)-mono_UVmuHist.UVmuHist(index,:);
monoRotError=[monoRotError_SV,monoRotError_MH,monoRotError_UV];

index=7:9;
monoAccelError_SV=truth_SVmuHist.xHist_SV(index,:)-mono_SVmuHist.SVmuHist(index,:);
monoAccelError_MH=truth_MHmuHist.xHist_MH(index,:)-mono_MHmuHist.MHmuHist(index,:);
monoAccelError_UV=truth_UVmuHist.xHist_UV(index,:)-mono_UVmuHist.UVmuHist(index,:);
monoAccelError=[monoAccelError_SV,monoAccelError_MH,monoAccelError_UV];

index=10:12;
monoAngAccelError_SV=truth_SVmuHist.xHist_SV(index,:)-mono_SVmuHist.SVmuHist(index,:);
monoAngAccelError_MH=truth_MHmuHist.xHist_MH(index,:)-mono_MHmuHist.MHmuHist(index,:);
monoAngAccelError_UV=truth_UVmuHist.xHist_UV(index,:)-mono_UVmuHist.UVmuHist(index,:);
monoAngAccelError=[monoAngAccelError_SV,monoAngAccelError_MH,monoAngAccelError_UV];

monoNRMSE=sqrt(sum(sum(monoPosError.^2+100*monoRotError.^2+monoAccelError.^2+100*monoAngAccelError.^2))/(length(monoAngAccelError)*12));

% joint estimation
index=1:3;
jointPosError_SV=truth_SVmuHist.xHist_SV(index,:)-joint_SVmuHist.SVmuHist(index,:);
jointPosError_MH=truth_MHmuHist.xHist_MH(index,:)-joint_MHmuHist.MHmuHist(index,:);
jointPosError_UV=truth_UVmuHist.xHist_UV(index,:)-joint_UVmuHist.UVmuHist(index,:);
jointPosError=[jointPosError_SV,jointPosError_MH,jointPosError_UV];

index=4:6;
jointRotError_SV=truth_SVmuHist.xHist_SV(index,:)-joint_SVmuHist.SVmuHist(index,:);
jointRotError_MH=truth_MHmuHist.xHist_MH(index,:)-joint_MHmuHist.MHmuHist(index,:);
jointRotError_UV=truth_UVmuHist.xHist_UV(index,:)-joint_UVmuHist.UVmuHist(index,:);
jointRotError=[jointRotError_SV,jointRotError_MH,jointRotError_UV];

index=7:9;
jointAccelError_SV=truth_SVmuHist.xHist_SV(index,:)-joint_SVmuHist.SVmuHist(index,:);
jointAccelError_MH=truth_MHmuHist.xHist_MH(index,:)-joint_MHmuHist.MHmuHist(index,:);
jointAccelError_UV=truth_UVmuHist.xHist_UV(index,:)-joint_UVmuHist.UVmuHist(index,:);
jointAccelError=[jointAccelError_SV,jointAccelError_MH,jointAccelError_UV];

index=10:12;
jointAngAccelError_SV=truth_SVmuHist.xHist_SV(index,:)-joint_SVmuHist.SVmuHist(index,:);
jointAngAccelError_MH=truth_MHmuHist.xHist_MH(index,:)-joint_MHmuHist.MHmuHist(index,:);
jointAngAccelError_UV=truth_UVmuHist.xHist_UV(index,:)-joint_UVmuHist.UVmuHist(index,:);
jointAngAccelError=[jointAngAccelError_SV,jointAngAccelError_MH,jointAngAccelError_UV];

jointNRMSE=sqrt(sum(sum(jointPosError.^2+100*jointRotError.^2+jointAccelError.^2+100*jointAngAccelError.^2))/(length(jointAngAccelError)*12));

% print out the result
disp('|--------------- ERROR REPORT ----------------')
disp("| Individual Estimator (RMSE): "+IndivNRMSE)
disp("| Monolithic Estimator (RMSE): "+monoNRMSE)
disp("| Joint Estimator (RMSE): "+jointNRMSE)

%% Compare position data
% Surface Vessel

figure
hold on
plot(tHist,truth_SVmuHist.xHist_SV(1:3,:))
plot(tHist,indiv_SVmuHist.SVmuHist(1:3,:),'--')
plot(tHist,mono_SVmuHist.SVmuHist(1:3,:),'--')
plot(tHist,joint_SVmuHist.SVmuHist(1:3,:),'--')
title('Surface Vessel')
xlabel('time [s]')
ylabel('Position [m] in NED')

% Multirotor Helicopter
figure
hold on
plot(tHist,truth_MHmuHist.xHist_MH(1:3,:))
plot(tHist,indiv_MHmuHist.MHmuHist(1:3,:),'--')
plot(tHist,mono_MHmuHist.MHmuHist(1:3,:),'--')
plot(tHist,joint_MHmuHist.MHmuHist(1:3,:),'--')
title('Multirotor Helicopter')
xlabel('time [s]')
ylabel('Position [m] in NED')

% Underwater Vehicle
figure
hold on
plot(tHist,truth_UVmuHist.xHist_UV(1:3,:))
plot(tHist,indiv_UVmuHist.UVmuHist(1:3,:),'--')
plot(tHist,mono_UVmuHist.UVmuHist(1:3,:),'--')
plot(tHist,joint_UVmuHist.UVmuHist(1:3,:),'--')
title('Underwater Vehicle')
xlabel('time [s]')
ylabel('Position [m] in NED')

%% animation
%init video writer
if strcmp(createVideo, 'true')
    vid = VideoWriter('Estimation_comp.mp4','MPEG-4');
    vid.FrameRate = length(tHist)/tHist(end); % regulate framerate so simtime = realtime
    vid.Quality = 100;
    open(vid);
end

animfig=figure('Name','Animation','Position', [10 10 1600 900]);
sgtitle('')

%init zoomed tracking view
zoomed=10; % control zoom level here

SVSubplotObj=subplot(3,3,3);
title('Tracking camera for Surface Vessel')
MHSubplotObj=subplot(3,3,6);
title('Tracking camera for Multirotor Helicopter')
UVSubplotObj=subplot(3,3,9);
title('Tracking camera for Underwater Vehicle')

mainSubplotObj=subplot(3,3,[1,2,4,5,7,8]);


% init animation objects
    % visualize water level
        [x y] = meshgrid(-200:10:200); 
        z = zeros(size(x, 1)); 
        oceanObj=surf(x, y, z,'FaceAlpha',0.3); % Plot the surface
        hold on
    
    % beacons
        rPNn_all=BeaconLoc();
        temp=size(rPNn_all);
        beaconCount=temp(2);
        data = stlread('beacon.stl');
        for i=1:beaconCount
            beaconObj(i) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(beaconObj(i),0.1);
            beaconObj(i).Vertices=beaconObj(i).Vertices.*0.15; % scale
            beaconObj(i).FaceColor='yellow'; % color
            beaconObj(i).LineWidth=0.08;
            beaconObj(i).LineStyle=':';
            tform = rigid3d(eye(3),rPNn_all(:,i).');
            beaconObj(i).Vertices = transformPointsForward(tform,beaconObj(i).Vertices);
        end
    
    %surface vessel
        data = stlread('boat.stl');
        boatObj(1) = trimesh(data,'FaceColor','none','EdgeColor','k');
        reducepatch(boatObj(1),0.008);
        boatObj(1).Vertices=boatObj(1).Vertices.*0.1; % scale
        boatObj(1).FaceColor='black'; % color
        boatObj(1).FaceAlpha=0.2;
        boatObj(1).LineWidth=0.8;
        boatObj(1).LineStyle=':';
        R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
            sind(-90),cosd(-90),0;...
            0,0,1];
        tform = rigid3d(R,[0,0,0]);
        boatObj(1).Vertices=transformPointsForward(tform,boatObj(1).Vertices);  
        boatShape=boatObj(1).Vertices; % save shape
        % estimator shadow
            % indiv
            boatObj(2) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(boatObj(2),0.008);
            boatObj(2).Vertices=boatObj(2).Vertices.*0.1; % scale
            boatObj(2).FaceColor='red'; % color
            boatObj(2).FaceAlpha=0.2;
            boatObj(2).LineWidth=0.08;
            boatObj(2).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            boatObj(2).Vertices=transformPointsForward(tform,boatObj(2).Vertices);  
            % mono
            boatObj(3) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(boatObj(3),0.008);
            boatObj(3).Vertices=boatObj(3).Vertices.*0.1; % scale
            boatObj(3).FaceColor='green'; % color
            boatObj(3).FaceAlpha=0.2;
            boatObj(3).LineWidth=0.08;
            boatObj(3).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            boatObj(3).Vertices=transformPointsForward(tform,boatObj(3).Vertices);  
            % joint
            boatObj(4) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(boatObj(4),0.008);
            boatObj(4).Vertices=boatObj(3).Vertices.*0.1; % scale
            boatObj(4).FaceColor='blue'; % color
            boatObj(4).FaceAlpha=0.2;
            boatObj(4).LineWidth=0.08;
            boatObj(4).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            boatObj(4).Vertices=transformPointsForward(tform,boatObj(4).Vertices);
            
        %   tracking cam
        copyobj(oceanObj,SVSubplotObj);
        for i2=1:4
            copyobj(beaconObj(i2),SVSubplotObj)
            trackedBoatObj(i2)=copyobj(boatObj(i2),SVSubplotObj);
        end
        
    % underwater vessel
        data = stlread('sub.stl');
        subObj(1) = trimesh(data,'FaceColor','none','EdgeColor','k');
        reducepatch(subObj(1),0.008);
        subObj(1).Vertices=subObj(1).Vertices.*0.1; % scale
        subObj(1).FaceColor='black'; % color
        subObj(1).FaceAlpha=0.2;
        subObj(1).LineWidth=0.08;
        subObj(1).LineStyle=':';
        R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
            sind(-90),cosd(-90),0;...
            0,0,1];
        tform = rigid3d(R,[0,0,0]);
        subObj(1).Vertices=transformPointsForward(tform,subObj(1).Vertices); 
        subShape=subObj(1).Vertices; % save shape
        % estimator shadow
            % indiv
            subObj(2) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(subObj(2),0.008);
            subObj(2).Vertices=subObj(2).Vertices.*0.1; % scale
            subObj(2).FaceColor='red'; % color
            subObj(2).FaceAlpha=0.2;
            subObj(2).LineWidth=0.08;
            subObj(2).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            subObj(2).Vertices=transformPointsForward(tform,subObj(2).Vertices); 
            % mono
            subObj(3) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(subObj(3),0.008);
            subObj(3).Vertices=subObj(3).Vertices.*0.1; % scale
            subObj(3).FaceColor='green'; % color
            subObj(3).FaceAlpha=0.2;
            subObj(3).LineWidth=0.08;
            subObj(3).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            subObj(3).Vertices=transformPointsForward(tform,subObj(3).Vertices); 
            % joint
            subObj(4) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(subObj(4),0.008);
            subObj(4).Vertices=subObj(4).Vertices.*0.1; % scale
            subObj(4).FaceColor='blue'; % color
            subObj(4).FaceAlpha=0.2;
            subObj(4).LineWidth=0.08;
            subObj(4).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            subObj(4).Vertices=transformPointsForward(tform,subObj(4).Vertices); 
            %   tracking cam
            copyobj(oceanObj,UVSubplotObj);
            for i2=1:4
                copyobj(beaconObj(i2),UVSubplotObj)
                trackedSubObj(i2)=copyobj(subObj(i2),UVSubplotObj);
            end
            
    % multirotor heli
        data = stlread('drone.stl');
        droneObj(1) = trimesh(data,'FaceColor','none','EdgeColor','k');
        reducepatch(droneObj(1),0.008);
        droneObj(1).Vertices=droneObj(1).Vertices.*0.02; % scale
        droneObj(1).FaceColor='black'; % color
        droneObj(1).FaceAlpha=0.2;
        droneObj(1).LineWidth=0.08;
        droneObj(1).LineStyle=':';
        R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
            sind(-90),cosd(-90),0;...
            0,0,1];
        tform = rigid3d(R,[0,0,0]);
        droneObj(1).Vertices=transformPointsForward(tform,droneObj(1).Vertices); 
        droneShape=droneObj(1).Vertices; % save shape
        % estimator shadow
            % indiv
            droneObj(2) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(droneObj(2),0.008);
            droneObj(2).Vertices=droneObj(2).Vertices.*0.02; % scale
            droneObj(2).FaceColor='red'; % color
            droneObj(2).FaceAlpha=0.2;
            droneObj(2).LineWidth=0.08;
            droneObj(2).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            droneObj(2).Vertices=transformPointsForward(tform,droneObj(2).Vertices);
            % mono
            droneObj(3) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(droneObj(3),0.008);
            droneObj(3).Vertices=droneObj(3).Vertices.*0.02; % scale
            droneObj(3).FaceColor='green'; % color
            droneObj(3).FaceAlpha=0.2;
            droneObj(3).LineWidth=0.08;
            droneObj(3).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            droneObj(3).Vertices=transformPointsForward(tform,droneObj(3).Vertices); 
            % joint
            droneObj(4) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(droneObj(4),0.008);
            droneObj(4).Vertices=droneObj(4).Vertices.*0.02; % scale
            droneObj(4).FaceColor='blue'; % color
            droneObj(4).FaceAlpha=0.2;
            droneObj(4).LineWidth=0.08;
            droneObj(4).LineStyle=':';
            R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
                sind(-90),cosd(-90),0;...
                0,0,1];
            tform = rigid3d(R,[0,0,0]);
            droneObj(4).Vertices=transformPointsForward(tform,droneObj(4).Vertices); 
        %   tracking cam
        copyobj(oceanObj,MHSubplotObj);
        for i2=1:4
            copyobj(beaconObj(i2),MHSubplotObj)
            trackedDroneObj(i2)=copyobj(droneObj(i2),MHSubplotObj);
        end
            
    % frame label
        axis([-90 90 -90 90 -90 90])
        frame_label = text(0,0,20, '');
        xlabel('x [m]')
        ylabel('y [m]')
        
	% annotations
        annotation('textbox', [0.91, 0.85, 0.1, 0.1], 'String', "Black: Truth"+newline+"Red: Individual Est"+newline+"Green: Mono Est"+newline+"Blue: Joint Est")
        SVErrorAnno=annotation('textbox',[0.91, 0.75, 0.08, 0.08],'string',"");
        MHErrorAnno=annotation('textbox',[0.91, 0.45, 0.08, 0.08],'string',"");
        UVErrorAnno=annotation('textbox',[0.91, 0.15, 0.08, 0.08],'string',"");

% animation loop
drawnow
pause(1);
try
    for i = 1:length(tHist)
        tic
        % draw boat
            % main plot
                DrawStl(truth_SVmuHist.xHist_SV(1:6,i),boatObj(1),boatShape);
                DrawStl(indiv_SVmuHist.SVmuHist(1:6,i),boatObj(2),boatShape);
                DrawStl(mono_SVmuHist.SVmuHist(1:6,i),boatObj(3),boatShape);
                DrawStl(joint_SVmuHist.SVmuHist(1:6,i),boatObj(4),boatShape);
                
            % tracking cam
                DrawStl(truth_SVmuHist.xHist_SV(1:6,i),trackedBoatObj(1),boatShape);
                DrawStl(indiv_SVmuHist.SVmuHist(1:6,i),trackedBoatObj(2),boatShape);
                DrawStl(mono_SVmuHist.SVmuHist(1:6,i),trackedBoatObj(3),boatShape);
                DrawStl(joint_SVmuHist.SVmuHist(1:6,i),trackedBoatObj(4),boatShape);
                set(SVSubplotObj, 'XLim', [-zoomed, zoomed]+truth_SVmuHist.xHist_SV(1,i))
                set(SVSubplotObj, 'YLim', [-zoomed, zoomed]+truth_SVmuHist.xHist_SV(2,i))
                set(SVSubplotObj, 'ZLim', [-zoomed, zoomed]+truth_SVmuHist.xHist_SV(3,i))
        
        % draw sub
            % main plot
                DrawStl(truth_UVmuHist.xHist_UV(1:6,i),subObj(1),subShape);
                DrawStl(indiv_UVmuHist.UVmuHist(1:6,i),subObj(2),subShape);
                DrawStl(mono_UVmuHist.UVmuHist(1:6,i),subObj(3),subShape);
                DrawStl(joint_UVmuHist.UVmuHist(1:6,i),subObj(4),subShape);
        
            % tracking cam
                DrawStl(truth_UVmuHist.xHist_UV(1:6,i),trackedSubObj(1),subShape);
                DrawStl(indiv_UVmuHist.UVmuHist(1:6,i),trackedSubObj(2),subShape);
                DrawStl(mono_UVmuHist.UVmuHist(1:6,i),trackedSubObj(3),subShape);
                DrawStl(joint_UVmuHist.UVmuHist(1:6,i),trackedSubObj(4),subShape);
                set(UVSubplotObj, 'XLim', [-zoomed, zoomed]+truth_UVmuHist.xHist_UV(1,i))
                set(UVSubplotObj, 'YLim', [-zoomed, zoomed]+truth_UVmuHist.xHist_UV(2,i))
                set(UVSubplotObj, 'ZLim', [-zoomed, zoomed+30]+truth_UVmuHist.xHist_UV(3,i))

        % draw quadcopter
            % main plot
                DrawStl(truth_MHmuHist.xHist_MH(1:6,i),droneObj(1),droneShape);
                DrawStl(indiv_MHmuHist.MHmuHist(1:6,i),droneObj(2),droneShape);
                DrawStl(mono_MHmuHist.MHmuHist(1:6,i),droneObj(3),droneShape);
                DrawStl(joint_MHmuHist.MHmuHist(1:6,i),droneObj(4),droneShape);
                
            % tracking cam   
                DrawStl(truth_MHmuHist.xHist_MH(1:6,i),trackedDroneObj(1),droneShape);
                DrawStl(indiv_MHmuHist.MHmuHist(1:6,i),trackedDroneObj(2),droneShape);
                DrawStl(mono_MHmuHist.MHmuHist(1:6,i),trackedDroneObj(3),droneShape);
                DrawStl(joint_MHmuHist.MHmuHist(1:6,i),trackedDroneObj(4),droneShape);
                set(MHSubplotObj, 'XLim', [-zoomed, zoomed]+truth_MHmuHist.xHist_MH(1,i))
                set(MHSubplotObj, 'YLim', [-zoomed, zoomed]+truth_MHmuHist.xHist_MH(2,i))
                set(MHSubplotObj, 'ZLim', [-zoomed-40, zoomed]+truth_MHmuHist.xHist_MH(3,i))
        
        % update error annotation
        SVErrorAnno.String="Total Abs Err"+newline+"Ind: "+string(sum(abs(truth_SVmuHist.xHist_SV(:,i)-indiv_SVmuHist.SVmuHist(:,i))))+newline+...
                            "Mono: "+string(sum(abs(truth_SVmuHist.xHist_SV(:,i)-mono_SVmuHist.SVmuHist(:,i))))+newline+...
                            "Joint: "+string(sum(abs(truth_SVmuHist.xHist_SV(:,i)-joint_SVmuHist.SVmuHist(:,i))));
        MHErrorAnno.String="Total Abs Err"+newline+"Ind: "+string(sum(abs(truth_MHmuHist.xHist_MH(:,i)-indiv_MHmuHist.MHmuHist(:,i))))+newline+...
                            "Mono: "+string(sum(abs(truth_MHmuHist.xHist_MH(:,i)-mono_MHmuHist.MHmuHist(:,i))))+newline+...
                            "Joint: "+string(sum(abs(truth_MHmuHist.xHist_MH(:,i)-joint_MHmuHist.MHmuHist(:,i))));
        UVErrorAnno.String="Total Abs Err"+newline+"Ind: "+string(sum(abs(truth_UVmuHist.xHist_UV(:,i)-indiv_UVmuHist.UVmuHist(:,i))))+newline+...
                            "Mono: "+string(sum(abs(truth_UVmuHist.xHist_UV(:,i)-mono_UVmuHist.UVmuHist(:,i))))+newline+...
                            "Joint: "+string(sum(abs(truth_UVmuHist.xHist_UV(:,i)-joint_UVmuHist.UVmuHist(:,i))));
        
        frame_label.String="T: "+num2str(tHist(i))+" [s]";
        

        % pause if the processing time is under step time
        pause(max(dt-toc,0.0001));
        if strcmp(createVideo, 'true')
             writeVideo(vid,getframe(animfig));
        end
    end
end

if strcmp(createVideo, 'true')
    close(vid)
end