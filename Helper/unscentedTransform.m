function [muy,Syy] = unscentedTransform(mux,Sxx,h,c)

%
% Square Root Unscented Transform of y = h(x) + v, where v ~ N(0,R)
% using 2*n + 1 sigma points and enforce sigma point feasibility, x <-- c(x)
%

%
% Input arguments
%
% mux: mean of x
% Sxx: upper triangular matrix such that Sxx.'*Sxx is the covariance of x
% h:   function handle for model
% c:   function handle to enforce sigma point feasibility (optional)
%
% Output arguments
%
% muy: mean of y
% Syy: upper triangular matrix such that Syy.'*Syy is the covariance of y
%

% Evaluate model at x = mux and obtain the following:
% muy: mean of y
% SR:  upper triangular matrix such that SR.'*SR is the covariance of the additive noise R
% C:   Jacobian matrix dh/dx evaluated at x = mux

assert(isequal(Sxx,triu(Sxx)),'Expected upper triangular matrix');
n = length(mux);          	% Length of input vector
nsigma = 2*n+1;             % Number of sigma points

% Unscented transform parameters
alpha = 1;
kappa = 0;
lambda = alpha^2*(n + kappa) - n;
gamma = realsqrt(n + lambda);
beta = 2;

% Mean weights
Wm = [repmat(1/(2*(n+lambda)),1,2*n), lambda/(n+lambda)];

% Covariance weights
Wc = [repmat(1/(2*(n+lambda)),1,2*n), lambda/(n+lambda) + (1-alpha^2+beta)];

% Generate sigma points
xsigma = zeros(n,nsigma);   % Input sigma points
for i = 1:n
    xsigma(:,i)   = mux + gamma*Sxx(i,:).';
    xsigma(:,i+n) = mux - gamma*Sxx(i,:).';
end
xsigma(:,2*n+1) = mux;

% Apply constraints
if nargin >= 4
    for i = 1:nsigma
        xsigma(:,i) = c(xsigma(:,i));
    end
end

% Transform the sigma points through the function
[muy,SR] = h(xsigma(:,nsigma));         % Use function eval at mean to extract SR and
assert(isequal(SR,triu(SR)),'Expected upper triangular matrix');
ny = length(muy);                       % determine output dimension and
ysigma = zeros(ny,nsigma);              % initialise output sigma points
ysigma(:,nsigma) = muy;
for i = 1:nsigma-1
    ysigma(:,i) = h(xsigma(:,i));
end

% Unscented mean
muy = sum(Wm.*ysigma,2);

% Compute conditional mean and sqrt covariance
dysigma = [realsqrt(Wc).*(ysigma - muy), SR];
R = triu(qr(dysigma.',0));
Syy = R(1:ny,:);
