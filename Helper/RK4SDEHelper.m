function [xnext,S,J] = RK4SDEHelper(f,xdw,u,param,dt)

% This helper function evaluates the map
% [ x[k] ] |---> [ x[k+1] ]
% [  dw  ]
% for one step of RK4 integration so that an affine or unscented transform can
% be used to map
% (mu[k],S[k]) |---> (mu[k+1],S[k+1])
%

%
% Input arguments
%
% f:        function handle for continuous-time model
% xdw:      state augmented with noise increment [x; dw]
% u:        input
% param:   	parameters to pass to model
% dt:       time step
%
% Output arguments
%
% xnext:   	state at time index k+1
% S:        upper triangular matrix such that S.'*S is the discrete-time process noise covariance (always zero since the noise is introduced by augmentation)
% J:        Jacobian matrix
% 

assert(mod(length(xdw),2) == 0)
nx = length(xdw)/2;
x = xdw(1:nx);
dw = xdw(nx+1:2*nx);

if nargout < 3
    % Assume the input, u, is constant over the time step (ZOH)
    f1 = f(x, u, param);
    f2 = f(x + (f1*dt + dw)/2, u, param);
    f3 = f(x + (f2*dt + dw)/2, u, param);
    f4 = f(x + f3*dt + dw, u, param);
else
    % Assume the input, u, is constant over the time step (ZOH)
    [f1,~,J1] = f(x, u, param);
    [f2,~,J2] = f(x + (f1*dt + dw)/2, u, param);
    [f3,~,J3] = f(x + (f2*dt + dw)/2, u, param);
    [f4,~,J4] = f(x + f3*dt + dw, u, param);

    % Partial derivatives of f# w.r.t. x
    df1dx = J1;
    df2dx = J2*(eye(nx) + df1dx*dt/2);
    df3dx = J3*(eye(nx) + df2dx*dt/2);
    df4dx = J4*(eye(nx) + df3dx*dt);

    % Partial derivatives of f# w.r.t. dw
    df1ddw = zeros(nx);
    df2ddw = J2*(df1ddw*dt + eye(nx))/2;
    df3ddw = J3*(df2ddw*dt + eye(nx))/2;
    df4ddw = J4*(df3ddw*dt + eye(nx));

    % Partial derivatives of xnext w.r.t. x
    Jx = eye(nx) + (df1dx + 2*df2dx + 2*df3dx + df4dx)*dt/6;

    % Partial derivatives of xnext w.r.t. dw
    Jdw = (df1ddw + 2*df2ddw + 2*df3ddw + df4ddw)*dt/6 + eye(nx);

    % Partial derivatives of xnext w.r.t. [x; dw]
    J = [Jx, Jdw];
end

xnext = x + (f1 + 2*f2 + 2*f3 + f4)*dt/6 + dw;

% No added noise, since we used augmentation instead
S = zeros(nx);