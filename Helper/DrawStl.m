function DrawStl(eta,obj,shape)
%DRAWBOAT Summary of this function goes here
%   Detailed explanation goes here

%% draw boat via points
    
    

    Rx=[1,0,0;...
        0,cos(eta(4)),-sin(eta(4));...
        0,sin(eta(4)),cos(eta(4))];
    Ry=[cos(-eta(5)),0,sin(-eta(5));...
        0,1,0;...
        -sin(-eta(5)),0,cos(-eta(5))];
    Rz=[cos(eta(6)),-sin(eta(6)),0;...
        sin(eta(6)),cos(eta(6)),0;...
        0,0,1];

    Rnb=Rz*Ry*Rx;
    
    tform = rigid3d(Rnb,eta(1:3).');
    obj.Vertices = transformPointsForward(tform,shape);

end

