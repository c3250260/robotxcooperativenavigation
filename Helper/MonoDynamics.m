function dx = MonoDynamics(t, x, u, param)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

dx_SV=SVDynamics(t, x(1:13-1), u(1:2), param.SV);
dx_MH=MHDynamics(t, x(1+13:13+13-1),u(3:6),param.MH);
dx_UV=UVDynamics(t, x(1+13*2:13+13*2-1),u(7:12),param.UV);

dx=[dx_SV;dx_MH;dx_UV];

