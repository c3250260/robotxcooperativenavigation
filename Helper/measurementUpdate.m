function [mu,S] = measurementUpdate(mu,S,u,y,measurementModel,param,transformMethod,makeStateFeasible)

%
% Condition the Gaussian pdf p(x) on measurement y to obtain p(x|y)
%

%
% Input arguments
%
% mu:	state mean
% S:    upper triangular matrix such that S.'*S is the covariance of the state
% u:    input
% y:    measurement
% measurementModel:     function handle to measurement model
% param:                parameters to pass to measurement model
% transformMethod:      'affine' or 'unscented' transform
% makeStateFeasible: 	function handle to enforce state constraints (unscented transform only, optional)
%
% Output arguments
%
% mu:   state mean conditioned on measurement
% S:    upper triangular matrix such that S.'*S is the covariance of the state conditioned on the measurement
%

switch transformMethod
    case 'affine'
        transform = @affineTransform;
    case 'unscented'
        transform = @unscentedTransform;
    otherwise
        error('Expecting transformMethod to be ''affine'' or ''unscented''');
end
if nargin < 8
    makeStateFeasible = @(x,param) x;
end

% Apply transform through the measurement model augmented with
% the identity function
%
% (mux,Pxx) |---> ([ muy ] [Pyy+R Pyx])
%                 ([ mux ],[Pxy   Pxx])
%
jointFunc = @(x) augmentIdentityAdapter(measurementModel,x,u,param);
[muyx,Syx] = transform(mu,S,jointFunc);

% Condition measurement and state prediction joint Gaussian on current
% measurement
[mu,S] = conditionGaussianOnMarginal(muyx,Syx,y);

% Enforce mean feasibility
mu = makeStateFeasible(mu,param);
