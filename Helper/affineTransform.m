function [muy,Syy] = affineTransform(mux,Sxx,h,~)

%
% Square Root Affine Transform of y = h(x) + v, where v ~ N(0,R)
%

%
% Input arguments
%
% mux: mean of x
% Sxx: upper triangular matrix such that Sxx.'*Sxx is the covariance of x
% h:   function handle for model
%
% Output arguments
%
% muy: mean of y
% Syy: upper triangular matrix such that Syy.'*Syy is the covariance of y
%

% Evaluate model at x = mux and obtain the following:
% muy: mean of y
% SR:  upper triangular matrix such that SR.'*SR is the covariance of the additive noise R
% C:   Jacobian matrix dh/dx evaluated at x = mux

assert(isequal(Sxx,triu(Sxx)),'Expected upper triangular matrix');
[muy,SR,C] = h(mux);
assert(isequal(SR,triu(SR)),'Expected upper triangular matrix');
ny = length(muy);

R = triu(qr([Sxx*C.';SR],0));
Syy = R(1:ny,:);
