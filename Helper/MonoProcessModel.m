function [f,SQ,A] = MonoProcessModel(x,u,param)

if nargout > 2
    % Evaluate f(x,u) and its gradient A = df/dx
    [f,A] = MonoDynamics(0,x,u,param);
else
    % Evaluate f(x,u)
    f = MonoDynamics(0,x,u,param);
end

% insert gyro bias back
f = [f(1:12);0;f(1+12:12+12);0;f(1+12*2:12+12*2);0];
if nargout > 2
    A = blkdiag(A,0);
end


% SQ
[~,SQ_SV] = SVProcessModel(x(1:13),u(1:2),param.SV);
[~,SQ_MH] = MHProcessModel(x(1+13:13+13),u(3:6),param.MH);
[~,SQ_UV] = UVProcessModel(x(1+13*2:13+13*2),u(7:12),param.UV);
SQ = blkdiag(SQ_SV,SQ_MH,SQ_UV); % SQ_Mono


assert(isequal(SQ,triu(SQ)),'Expect SQ to be upper triangular');