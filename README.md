# RobotX Coop Navigation Project




## Usage
### main.m 
Runs the simulation and logs result in the Logs folder.

uncommenting state estimator to 'none'/'individual'/'monolithic' or 'joint' to select estimator

setting UseSavedMPC to 'true' will bypass the MPC optimization step, and instead uses previously calculated actuation commands stored in Logs file. If the control dynamics has been changed or the ode solver is acting weird, causing the system to become unstable, set this back to 'false'.

### Estimator_comparison.m
Compares result from different estimators based on the logged data in the Logs folder

## File structure
vehicle dynamics, sensor models, parameters and STL models used by a single vehicle is stored in their respective folder. Mainly:

- Multirotor_Helicopter
- Surface_Vessel
- Underwater_Vehicle

Logged simulation data, including truth, estimation and actuation are stored in

- Logs

General Helper functions and beacons are in

- Helper

