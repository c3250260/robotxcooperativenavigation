% RobotX coop navigation project for MCHA4100 (S2 2021)
% by Kwan Nok Mak (3250260)
% Demo: 28/10/2021
%
%

clc
clear
close all

addpath Underwater_Vehicle
addpath Surface_Vessel
addpath Multirotor_Helicopter
addpath Helper
addpath Logs

%load params
SVParam=SVParams();
UVParam=UVParams();
MHParam=MHParams();
MonoParam.SV=SVParam;
MonoParam.MH=MHParam;
MonoParam.UV=UVParam;


u_SV = [500;0];% Initial control action
u_UV = [0;0;0;0;0;0];
u_MH = [0;0;0;0];

tic 

%% simulation config
simTime= 30;%[s]
dt=0.05;%[s]

% % StateEstimator = 'none';
StateEstimator = 'individual';
% StateEstimator = 'monolithic';
% StateEstimator = 'joint';

UseSavedMPC = 'true';
% UseSavedMPC = 'false';

% inital cond
ic_SV=[0;0;0;0;0;0;...
         0;0;0;0;0;0];

ic_UV=[0;0;-5;0;0;0;...
        0;0;0;0;0;0]; 
    
ic_MH=[0;40;50;0;0;0;...
        0;0;0;0;0;0];

%% estimator inital cond
mu0_SV = [ic_SV;0.05];
S0_SV = 0.05*eye(13);

mu0_MH = [ic_MH;0.05];
S0_MH = 0.05*eye(13);

mu0_UV = [ic_UV;0.05];
S0_UV = 0.05*eye(13);

mu0_Mono = [mu0_SV;mu0_MH;mu0_UV];
S0_Mono = 0.05*eye(13*3);

options     = odeset('MaxStep',0.005);

%% run sim
%logging init
rPNn_all=BeaconLoc();
temp=size(rPNn_all);
beaconCount=temp(2);

U = [];
tHist = 0:dt:simTime; 
xHist_SV = zeros(12,length(tHist));
uHist_SV = zeros(length(u_SV),length(tHist));

% sensors log init
SVIMU_AccelHist = zeros(3,length(tHist));
SVIMU_GyroHist = zeros(3,length(tHist));
SVIMU_MagHist = zeros(3,length(tHist));
SVBearing_Hist = zeros(beaconCount,length(tHist));
SVGPS_Hist = zeros(3,length(tHist));

MHIMU_AccelHist = zeros(3,length(tHist));
MHIMU_GyroHist = zeros(3,length(tHist));
MHIMU_MagHist = zeros(3,length(tHist));
MHBearing_Hist = zeros(3,length(tHist));
MHLPS_Hist = zeros(1,length(tHist));
MHGPS_Hist = zeros(3,length(tHist));

UVIMU_AccelHist = zeros(3,length(tHist));
UVIMU_GyroHist = zeros(3,length(tHist));
UVIMU_MagHist = zeros(3,length(tHist));
UVPinger_Hist = zeros(3,length(tHist));

% state estimator init
SVmuHist = nan(13,length(tHist));
SVmuHist(:,1) = mu0_SV;
SVSHist = nan(13,13,length(tHist));
SVSHist(:,:,1) = S0_SV;

MHmuHist = nan(13,length(tHist));
MHmuHist(:,1) = mu0_MH;
MHSHist = nan(13,13,length(tHist));
MHSHist(:,:,1) = S0_MH;

UVmuHist = nan(13,length(tHist));
UVmuHist(:,1) = mu0_UV;
UVSHist = nan(13,13,length(tHist));
UVSHist(:,:,1) = S0_UV;

MonomuHist = nan(13*3,length(tHist));
MonomuHist(:,1) = mu0_Mono;
MonoSHist = nan(13*3,13*3,length(tHist));
MonoSHist(:,:,1) = S0_Mono;

JointmuHist = nan(13*3,length(tHist));
JointmuHist(:,1) = mu0_Mono;
JointSHist = nan(13*3,13*3,length(tHist));
JointSHist(:,:,1) = S0_Mono;

%set init cond
    %SV
    xHist_SV(:,1) = ic_SV;
    xHist_SV(13,:) = 0.1;   % true gyro bias
    uHist_SV(:,1) = u_SV;
	%MH
	xHist_MH(:,1) = ic_MH;
    xHist_MH(13,:) = 0.1;   % true gyro bias
    uHist_MH(:,1) = u_MH;
    %UV
	xHist_UV(:,1) = ic_UV;
    xHist_UV(13,:) = 0.1;   % true gyro bias
    uHist_UV(:,1) = u_UV;

% load actuation
if UseSavedMPC=="true"
	loaduHist=load('Logs/uHist_MH.mat');
	u_Load=loaduHist.uHist_MH;
end

%start main simulation loop
for i = 1:length(tHist)-1

	% Simulate one time step with ZOH input u
	func = @(t,x) SVDynamics(t,x,u_SV,SVParam);
	[~,xTemp_SV] = ode45(func,[tHist(i) tHist(i+1)],xHist_SV(1:12,i),options);
	func = @(t,x) MHDynamics(t,x,u_MH,MHParam);
	[~,xTemp_MH] = ode45(func,[tHist(i) tHist(i+1)],xHist_MH(1:12,i),options);
	func = @(t,x) UVDynamics(t,x,u_UV,UVParam);
	[~,xTemp_UV] = ode45(func,[tHist(i) tHist(i+1)],xHist_UV(1:12,i),options);

          
	% log ground truth data
    %SV
	xTemp_SV = xTemp_SV.';
	xHist_SV(1:12,i+1) = xTemp_SV(:,end).';
    uHist_SV(:,i+1)= u_SV;
	%MH
	xTemp_MH = xTemp_MH.';
	xHist_MH(1:12,i+1) = xTemp_MH(:,end).';
    uHist_MH(:,i+1)= u_MH;
    %UV
	xTemp_UV = xTemp_UV.';
	xHist_UV(1:12,i+1) = xTemp_UV(:,end).';
    uHist_UV(:,i+1)= u_UV;

    
	% Simulate sensor measurement
        % SV
            SVIMU_AccelHist(:,i) = simulateMeasurement(@SVAccelerometerMeasurementModel,xHist_SV(:,i),uHist_SV(:,i),SVParam);
            SVIMU_GyroHist(:,i) = simulateMeasurement(@SVGyroscopeMeasurementModel,xHist_SV(:,i),uHist_SV(:,i),SVParam);
            SVIMU_MagHist(:,i) = simulateMeasurement(@SVMagnetometerModel,xHist_SV(:,i),uHist_SV(:,i),SVParam);
            SVBearing_Hist(:,i) = simulateMeasurement(@SVVisualBearingMeasurementModel,xHist_SV(:,i),uHist_SV(:,i),SVParam);
            SVGPS_Hist(:,i) = simulateMeasurement(@SVGPSMeasurementModel,xHist_SV(:,i),uHist_SV(:,i),SVParam);
            
        % MH
            MHIMU_AccelHist(:,i) = simulateMeasurement(@MHAccelerometerMeasurementModel,xHist_MH(:,i),uHist_MH(:,i),MHParam);
            MHIMU_GyroHist(:,i) = simulateMeasurement(@MHGyroscopeMeasurementModel,xHist_MH(:,i),uHist_MH(:,i),MHParam);
            MHBearing_Hist(:,i) = simulateMeasurement(@MHVisualBearingMeasurementModel,[xHist_SV(:,i);xHist_MH(:,i);xHist_UV(:,i)],uHist_MH(:,i),MonoParam); %multi-vehicle
            MHLPS_Hist(:,i) = simulateMeasurement(@MHLPSModel,[xHist_SV(:,i);xHist_MH(:,i);xHist_UV(:,i)],uHist_MH(:,i),MonoParam);%multi-vehicle
            MHGPS_Hist(:,i) = simulateMeasurement(@MHGPSMeasurementModel,xHist_MH(:,i),uHist_MH(:,i),MHParam);
            
        % UV
            UVIMU_AccelHist(:,i) = simulateMeasurement(@UVAccelerometerMeasurementModel,xHist_UV(:,i),uHist_UV(:,i),UVParam);
            UVIMU_GyroHist(:,i) = simulateMeasurement(@UVGyroscopeMeasurementModel,xHist_UV(:,i),uHist_UV(:,i),UVParam);
            UVPinger_Hist(:,i) = simulateMeasurement(@UVPingerMeasurementModel,[xHist_SV(:,i);xHist_MH(:,i);xHist_UV(:,i)],uHist_MH(:,i),MonoParam);%multi-vehicle

            
    % State Estimation
	switch StateEstimator
        case 'none'
            % do nothing
            
        case 'individual'
                transformMethod='unscented';
            % SV
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVIMU_AccelHist(:,i),@SVAccelerometerMeasurementModel,SVParam,transformMethod);
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVIMU_GyroHist(:,i),@SVGyroscopeMeasurementModel,SVParam,transformMethod);
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVBearing_Hist(:,i),@SVVisualBearingMeasurementModel,SVParam,transformMethod);
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVGPS_Hist(:,i),@SVGPSMeasurementModel,SVParam,transformMethod);
                % Estimate next state
                [SVmuHist(:,i+1),SVSHist(:,:,i+1)] = SVTimeUpdateContinuous(SVmuHist(:,i),SVSHist(:,:,i),u_SV,@SVProcessModel,SVParam,dt,'RK4',transformMethod);
                
            % MH
                [MHmuHist(:,i),MHSHist(:,:,i)] = measurementUpdate(MHmuHist(:,i),MHSHist(:,:,i),u_MH,MHIMU_AccelHist(:,i),@MHAccelerometerMeasurementModel,MHParam,transformMethod);
                [MHmuHist(:,i),MHSHist(:,:,i)] = measurementUpdate(MHmuHist(:,i),MHSHist(:,:,i),u_MH,MHIMU_GyroHist(:,i),@MHGyroscopeMeasurementModel,MHParam,transformMethod);
                [MHmuHist(:,i),MHSHist(:,:,i)] = measurementUpdate(MHmuHist(:,i),MHSHist(:,:,i),u_MH,MHGPS_Hist(:,i),@MHGPSMeasurementModel,MHParam,transformMethod);
                % Estimate next state
                [MHmuHist(:,i+1),MHSHist(:,:,i+1)] = MHTimeUpdateContinuous(MHmuHist(:,i),MHSHist(:,:,i),u_MH,@MHProcessModel,MHParam,dt,'RK4',transformMethod);
                
            % UV
                [UVmuHist(:,i),UVSHist(:,:,i)] = measurementUpdate(UVmuHist(:,i),UVSHist(:,:,i),u_UV,UVIMU_AccelHist(:,i),@UVAccelerometerMeasurementModel,UVParam,transformMethod);
                [UVmuHist(:,i),UVSHist(:,:,i)] = measurementUpdate(UVmuHist(:,i),UVSHist(:,:,i),u_UV,UVIMU_GyroHist(:,i),@UVGyroscopeMeasurementModel,UVParam,transformMethod);
                % Estimate next state
                [UVmuHist(:,i+1),UVSHist(:,:,i+1)] = UVTimeUpdateContinuous(UVmuHist(:,i),UVSHist(:,:,i),u_UV,@UVProcessModel,UVParam,dt,'RK4',transformMethod);
                
        case 'monolithic'
            transformMethod='unscented';
            % concat states into one giant mess
            u_Mono=[u_SV;u_MH;u_UV];
            
            % estimate curret state
                % SV
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,SVIMU_AccelHist(:,i),@SVAccelerometerMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,SVIMU_GyroHist(:,i),@SVGyroscopeMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,SVGPS_Hist(:,i),@SVGPSMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,SVBearing_Hist(:,i),@SVVisualBearingMeasurementModel,MonoParam,transformMethod);
                % MH
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,MHIMU_AccelHist(:,i),@MHAccelerometerMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,MHIMU_GyroHist(:,i),@MHGyroscopeMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,MHGPS_Hist(:,i),@MHGPSMeasurementModel,MonoParam,transformMethod);
                % UV
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,UVIMU_AccelHist(:,i),@UVAccelerometerMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,UVIMU_GyroHist(:,i),@UVGyroscopeMeasurementModel,MonoParam,transformMethod);
                % between vehicles
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,UVPinger_Hist(:,i),@UVPingerMeasurementModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,MHLPS_Hist(:,i),@MHLPSModel,MonoParam,transformMethod);
                [MonomuHist(:,i),MonoSHist(:,:,i)] = measurementUpdate(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,MHBearing_Hist(:,i),@MHVisualBearingMeasurementModel,MonoParam,transformMethod);
                
            % Estimate next state
            [MonomuHist(:,i+1),MonoSHist(:,:,i+1)] = MonoTimeUpdateContinuous(MonomuHist(:,i),MonoSHist(:,:,i),u_Mono,@MonoProcessModel,MonoParam,dt,'RK4',transformMethod);
            
            % unpack mono states back to individual
            SVmuHist(:,i) = MonomuHist(1:13,i);
            MHmuHist(:,i) = MonomuHist(1+13:13+13,i);
            UVmuHist(:,i) = MonomuHist(1+13*2:13+13*2,i);
            
            SVmuHist(:,i+1) = MonomuHist(1:13,i+1);
            MHmuHist(:,i+1) = MonomuHist(1+13:13+13,i+1);
            UVmuHist(:,i+1) = MonomuHist(1+13*2:13+13*2,i+1);
            
        case 'joint'
            % refer to week 6 kf slide, last page
            
            transformMethod='unscented';
            % concat states into one giant mess
            u_Joint=[u_SV;u_MH;u_UV];

            % local vehicle estimation
            % SV
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVIMU_AccelHist(:,i),@SVAccelerometerMeasurementModel,SVParam,transformMethod);
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVIMU_GyroHist(:,i),@SVGyroscopeMeasurementModel,SVParam,transformMethod);
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVBearing_Hist(:,i),@SVVisualBearingMeasurementModel,SVParam,transformMethod);
                [SVmuHist(:,i),SVSHist(:,:,i)] = measurementUpdate(SVmuHist(:,i),SVSHist(:,:,i),u_SV,SVGPS_Hist(:,i),@SVGPSMeasurementModel,SVParam,transformMethod);
            % MH
                [MHmuHist(:,i),MHSHist(:,:,i)] = measurementUpdate(MHmuHist(:,i),MHSHist(:,:,i),u_MH,MHIMU_AccelHist(:,i),@MHAccelerometerMeasurementModel,MHParam,transformMethod);
                [MHmuHist(:,i),MHSHist(:,:,i)] = measurementUpdate(MHmuHist(:,i),MHSHist(:,:,i),u_MH,MHIMU_GyroHist(:,i),@MHGyroscopeMeasurementModel,MHParam,transformMethod);
                [MHmuHist(:,i),MHSHist(:,:,i)] = measurementUpdate(MHmuHist(:,i),MHSHist(:,:,i),u_MH,MHGPS_Hist(:,i),@MHGPSMeasurementModel,MHParam,transformMethod);
             
            % UV
                [UVmuHist(:,i),UVSHist(:,:,i)] = measurementUpdate(UVmuHist(:,i),UVSHist(:,:,i),u_UV,UVIMU_AccelHist(:,i),@UVAccelerometerMeasurementModel,UVParam,transformMethod);
                [UVmuHist(:,i),UVSHist(:,:,i)] = measurementUpdate(UVmuHist(:,i),UVSHist(:,:,i),u_UV,UVIMU_GyroHist(:,i),@UVGyroscopeMeasurementModel,UVParam,transformMethod);
                
            % merge estimation
            JointmuHist(:,i)=[SVmuHist(:,i);MHmuHist(:,i);UVmuHist(:,i)];
            JointSHist(:,:,i)=blkdiag(SVSHist(:,:,i),MHSHist(:,:,i),UVSHist(:,:,i));
            
            [JointmuHist(:,i),JointSHist(:,:,i)] = measurementUpdate(JointmuHist(:,i),JointSHist(:,:,i),u_Joint,UVPinger_Hist(:,i),@UVPingerMeasurementModel,MonoParam,transformMethod);
            [JointmuHist(:,i),JointSHist(:,:,i)] = measurementUpdate(JointmuHist(:,i),JointSHist(:,:,i),u_Joint,MHLPS_Hist(:,i),@MHLPSModel,MonoParam,transformMethod);
            [JointmuHist(:,i),JointSHist(:,:,i)] = measurementUpdate(JointmuHist(:,i),JointSHist(:,:,i),u_Joint,MHBearing_Hist(:,i),@MHVisualBearingMeasurementModel,MonoParam,transformMethod);
            
            % update local estimation
            SVSHist(:,:,i)=JointSHist(1:13,1:13,i);
            MHSHist(:,:,i)=JointSHist(1+13:13*2,1+13:13*2,i);
            UVSHist(:,:,i)=JointSHist(1+13*2:13*3,1+13*2:13*3,i);
            
            SVmuHist(:,i)=JointmuHist(1:13,i);
            MHmuHist(:,i)=JointmuHist(1+13:13*2,i);
            UVmuHist(:,i)=JointmuHist(1+13*2:13*3,i);
            
            % Estimate next state individually
            [SVmuHist(:,i+1),SVSHist(:,:,i+1)] = SVTimeUpdateContinuous(SVmuHist(:,i),SVSHist(:,:,i),u_SV,@SVProcessModel,SVParam,dt,'RK4',transformMethod);
            [MHmuHist(:,i+1),MHSHist(:,:,i+1)] = MHTimeUpdateContinuous(MHmuHist(:,i),MHSHist(:,:,i),u_MH,@MHProcessModel,MHParam,dt,'RK4',transformMethod);
            [UVmuHist(:,i+1),UVSHist(:,:,i+1)] = UVTimeUpdateContinuous(UVmuHist(:,i),UVSHist(:,:,i),u_UV,@UVProcessModel,UVParam,dt,'RK4',transformMethod);
          
                
        otherwise
        error('Expecting StateEstimator to be ''none'', ''individual'' , ''monolithic'' , ''joint''');
	end
    
        
        
    % controller
    u_SV=[sind(tHist(i)*10)*500;cosd(tHist(i)*10)*500]; %sin wave feed
    u_UV=[150;0;-150;0;0;80];
    
    switch UseSavedMPC % this is here to save computing time
        case 'true'
            u_MH = u_Load(:,i+1);
        case 'false'
            U = MHcontrolMPC(tHist(i+1),xHist_MH(1:12,i+1),u_MH,U,MHParam);
            u_MH = U(1:4);
        otherwise
        error('Expecting UseSavedMPC to be ''true'' or ''false''');
    end
    
    %verboos
    disp("T= "+i*dt+"/"+simTime)      
end

disp('------------------------------------------------')
disp('completed simulation')
elapsed=toc;
disp('time elapsed: '+ string(elapsed))

%% save data history
save('Logs/tHist.mat','tHist');

save('Logs/xHist_SV.mat','xHist_SV');
save('Logs/xHist_MH.mat','xHist_MH');
save('Logs/xHist_UV.mat','xHist_UV');

if UseSavedMPC == "false"
    save('Logs/uHist_SV.mat','uHist_SV');
    save('Logs/uHist_MH.mat','uHist_MH');
    save('Logs/uHist_UV.mat','uHist_UV');
end

switch StateEstimator
	case 'none'
	case 'individual'
        save('Logs/indiv_SVmuHist.mat','SVmuHist');
        save('Logs/indiv_MHmuHist.mat','MHmuHist');
        save('Logs/indiv_UVmuHist.mat','UVmuHist');
	case 'monolithic'
        save('Logs/mono_SVmuHist.mat','SVmuHist');
        save('Logs/mono_MHmuHist.mat','MHmuHist');
        save('Logs/mono_UVmuHist.mat','UVmuHist');
	case 'joint'
        save('Logs/joint_SVmuHist.mat','SVmuHist');
        save('Logs/joint_MHmuHist.mat','MHmuHist');
        save('Logs/joint_UVmuHist.mat','UVmuHist');
end

%% plot stuff
    % ground truth
        % SV
            figure('Name','Ground truth for Surface Vessel','NumberTitle','off','Position', [10 10 1400 900])
            sgtitle('Ground truth for Surface Vessel')
            subplot(2,2,1);
            hold on
            plot(tHist,xHist_SV(1:2,:));% N,E (truth)
            plot(tHist,SVmuHist(1:2,:),'--');% N,E (estimation)
            title('position')
            xlabel('Time [s]')
            ylabel('Pos [m]')
            legend('N (truth)','E (truth)','N (pred)','E (pred)')

            subplot(2,2,2);
            hold on
            plot(tHist,xHist_SV(6,:)*180/pi) % roll, yaw (truth)
            plot(tHist,SVmuHist(6,:)*180/pi,'--') % roll, yaw (estimation)
            title('pose')
            xlabel('Time [s]')
            ylabel('Angle [Deg]')
            legend('Yaw (truth)','Yaw (pred)')

            subplot(2,2,3)
            hold on
            plot(tHist,xHist_SV(7,:)) % dx
            plot(tHist,xHist_SV(8,:)) % dy
            title('Velocity')
            xlabel('Time [s]')
            ylabel('V [m/s]')
            legend('x','y')

            subplot(2,2,4)
            hold on
            plot(tHist,uHist_SV)
            xlabel('Time [s]')
            ylabel('u')
            title('Actuation')          
            
        % MH
            figure('Name','Ground truth for Multirotor Helicopter','NumberTitle','off','Position', [10 10 1400 900])
            sgtitle('Ground truth for Multirotor Helicopter')
            subplot(2,2,1);
            hold on
            plot(tHist,xHist_MH(1:3,:));  % N, E, D (truth)
            plot(tHist,MHmuHist(1:3,:),'--');  % N, E, D (estimation)
            title('position')
            xlabel('Time [s]')
            ylabel('Pos [m]')
            legend('N (truth)','E (truth)','D (truth)','N (pred)','E (pred)','D (pred)')
            
            subplot(2,2,2);
            hold on
            plot(tHist,xHist_MH(4:6,:)*180/pi) % roll pitch yaw (truth)
            plot(tHist,MHmuHist(4:6,:)*180/pi,'--');  % roll pitch yaw (estimation)
            title('pose')
            xlabel('Time [s]')
            ylabel('Angle [Deg]')
            legend('Roll','Pitch','Yaw','Roll (pred)','Pitch (pred)','Yaw (pred)')
            
            subplot(2,2,3)
            hold on
            plot(tHist,xHist_MH(7:9,:)) % dx dy dz
            title('Velocity')
            xlabel('Time [s]')
            ylabel('V [m/s]')
            legend('x','y','z')
            
            subplot(2,2,4)
            hold on
            plot(tHist,uHist_MH)
            xlabel('Time [s]')
            ylabel('u')
            title('Actuation')
            legend('prop 1','prop 2','prop 3','prop 4')
            
         % UV
            figure('Name','Ground truth for Underwater Vehicle','NumberTitle','off','Position', [10 10 1400 900])
            sgtitle('Ground truth for Underwater Vehicle')
            subplot(2,2,1);
            hold on
            plot(tHist,xHist_UV(1:3,:));  % N, E, D (truth)
            plot(tHist,UVmuHist(1:3,:),'--'); % N, E, D (estimation)
            title('position')
            xlabel('Time [s]')
            ylabel('Pos [m]')
            legend('N (truth)','E (truth)','D (truth)','N (pred)','E (pred)','D (pred)')
            
            subplot(2,2,2);
            hold on
            plot(tHist,xHist_UV(4:6,:)*180/pi) % roll pitch yaw(truth)
            plot(tHist,UVmuHist(4:6,:)*180/pi,'--');  % roll pitch yaw (estimation)
            title('pose')
            xlabel('Time [s]')
            ylabel('Angle [Deg]')
            legend('Roll','Pitch','Yaw')
            
            subplot(2,2,3)
            hold on
            plot(tHist,xHist_UV(7,:)) % dx
            plot(tHist,xHist_UV(8,:)) % dy
            plot(tHist,xHist_UV(9,:)) % dz
            title('Velocity')
            xlabel('Time [s]')
            ylabel('V [m/s]')
            legend('x','y','z')
            
            subplot(2,2,4)
            hold on
            plot(tHist,uHist_UV)
            xlabel('Time [s]')
            ylabel('u')
            title('Actuation')
            

            
    % plot raw sensor data
        % SV
        figure('Name','Sensor Measurement for Surface Vessel','NumberTitle','off','Position', [10 10 1400 900])
        sgtitle('Sensor Measurement for Surface Vessel')
            % IMU
                subplot(3,3,1); % Accel
                hold on
                plot(tHist,SVIMU_AccelHist) 
                title('IMU Acceleration')
                legend('x','y','z')
                xlabel('Time [s]')
                ylabel('Acceleration [m/s^2]')
                subplot(3,3,2); % gyro
                hold on
                plot(tHist,SVIMU_GyroHist*180/pi) 
                title('IMU Gyro')
                legend('roll','pitch','yaw')
                xlabel('Time [s]')
                ylabel('Angular vel [Deg/s]')
                subplot(3,3,3)  % magneto
                hold on
                plot(tHist,SVIMU_MagHist(3,:)*180/pi);
                title('IMU Magnetometer')
                xlabel('Time [s]')
                ylabel('Angle [Deg]')
            % bearing sensor
                subplot(3,3,4)
                hold on
                plot(tHist,SVBearing_Hist)
                title('Beacon bearing sensor')
                xlabel('Time [s]')
                ylabel('Angle [Deg]')
            % GPS
                subplot(3,3,7)
                plot(tHist(1:length(tHist)-1),SVGPS_Hist(1,1:length(tHist)-1))
                title('GPS [x] (ECEF Euclidean)')
                xlabel('Time [s]')
                ylabel('Location [m]')
                subplot(3,3,8)
                plot(tHist(1:length(tHist)-1),SVGPS_Hist(2,1:length(tHist)-1))
                title('GPS [y](ECEF Euclidean)')
                xlabel('Time [s]')
                ylabel('Location [m]')
                subplot(3,3,9)
                plot(tHist(1:length(tHist)-1),SVGPS_Hist(3,1:length(tHist)-1))
                title('GPS [z](ECEF Euclidean)')
                xlabel('Time [s]')
                ylabel('Location [m]')
        
        % MH
        figure('Name','Sensor Measurement for Multirotor Helicopter','NumberTitle','off','Position', [10 10 1400 900])
        sgtitle('Sensor Measurement for Multirotor Helicopter')
            % IMU
                subplot(3,3,1); % Accel
                hold on
                plot(tHist,MHIMU_AccelHist) 
                title('IMU Acceleration')
                legend('x','y','z')
                xlabel('Time [s]')
                ylabel('Acceleration [m/s^2]')
                subplot(3,3,2); % gyro
                hold on
                plot(tHist,MHIMU_GyroHist*180/pi) 
                title('IMU Gyro')
                legend('roll','pitch','yaw')
                xlabel('Time [s]')
                ylabel('Angular vel [Deg/s]')
            % bearing sensor
                subplot(3,3,4)
                hold on
                plot(tHist,MHBearing_Hist)
                title('bearing sensor (normalized vector)')
                xlabel('Time [s]')
                ylabel('Angle [Deg]')
            % radio LPS
                subplot(3,3,5)
                hold on
                plot(tHist,MHLPS_Hist)
                title('LPS beacon sensor')
                xlabel('Time [s]')
                ylabel('Distance [m]')
            % GPS
                subplot(3,3,7)
                plot(tHist(1:length(tHist)-1),MHGPS_Hist(1,1:length(tHist)-1))
                title('GPS [x] (ECEF Euclidean)')
                xlabel('Time [s]')
                ylabel('Location [m]')
                subplot(3,3,8)
                plot(tHist(1:length(tHist)-1),MHGPS_Hist(2,1:length(tHist)-1))
                title('GPS [y](ECEF Euclidean)')
                xlabel('Time [s]')
                ylabel('Location [m]')
                subplot(3,3,9)
                plot(tHist(1:length(tHist)-1),MHGPS_Hist(3,1:length(tHist)-1))
                title('GPS [z](ECEF Euclidean)')
                xlabel('Time [s]')
                ylabel('Location [m]')
                
        % UV
        figure('Name','Sensor Measurement for Underwater Vehicle','NumberTitle','off','Position', [10 10 1400 900])
        sgtitle('Sensor Measurement for Underwater Vehicle')
            % IMU
                subplot(2,3,1); % Accel
                hold on
                plot(tHist,UVIMU_AccelHist) 
                title('IMU Acceleration')
                legend('x','y','z')
                xlabel('Time [s]')
                ylabel('Acceleration [m/s^2]')
                subplot(2,3,2); % gyro
                hold on
                plot(tHist,UVIMU_GyroHist*180/pi) 
                title('IMU Gyro')
                legend('roll','pitch','yaw')
                xlabel('Time [s]')
                ylabel('Angular vel [Deg/s]')
            % Pinger
                subplot(2,3,4)
                hold on
                plot(tHist,UVPinger_Hist)
                legend('x','y','z')
                title('Pinger')
                xlabel('Time [s]')
                ylabel('Normalized Vector')
                




%% animation
figure('Name','Animation','Position', [10 10 1400 900])

% init animation objects
    % visualize water level
        [x y] = meshgrid(-200:50:200); 
        z = zeros(size(x, 1)); 
        surf(x, y, z,'FaceAlpha',0.3) % Plot the surface
        hold on
    
    % beacons
        rPNn_all=BeaconLoc();
        temp=size(rPNn_all);
        beaconCount=temp(2);
        data = stlread('beacon.stl');
        for i=1:beaconCount
            beaconObj(i) = trimesh(data,'FaceColor','none','EdgeColor','k');
            reducepatch(beaconObj(i),0.1);
            beaconObj(i).Vertices=beaconObj(i).Vertices.*0.15; % scale
            beaconObj(i).FaceColor='yellow'; % color
            beaconObj(i).LineWidth=0.2;
            beaconObj(i).LineStyle=':';
            tform = rigid3d(eye(3),rPNn_all(:,i).');
            beaconObj(i).Vertices = transformPointsForward(tform,beaconObj(i).Vertices);
        end
    
    %surface vessel
        data = stlread('boat.stl');
        boatObj = trimesh(data,'FaceColor','none','EdgeColor','k');
        reducepatch(boatObj,0.02);
        boatObj.Vertices=boatObj.Vertices.*0.1; % scale
        boatObj.FaceColor='red'; % color
        boatObj.LineWidth=0.2;
        boatObj.LineStyle=':';
        R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
            sind(-90),cosd(-90),0;...
            0,0,1];
        tform = rigid3d(R,[0,0,0]);
        boatObj.Vertices=transformPointsForward(tform,boatObj.Vertices);  
        boatShape=boatObj.Vertices; % save shape
        
    % underwater vessel
        data = stlread('sub.stl');
        subObj = trimesh(data,'FaceColor','none','EdgeColor','k');
        reducepatch(subObj,0.01);
        subObj.Vertices=subObj.Vertices.*0.1; % scale
        subObj.FaceColor='green'; % color
        subObj.LineWidth=0.2;
        subObj.LineStyle=':';
        R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
            sind(-90),cosd(-90),0;...
            0,0,1];
        tform = rigid3d(R,[0,0,0]);
        subObj.Vertices=transformPointsForward(tform,subObj.Vertices); 
        subShape=subObj.Vertices; % save shape
    
    % multirotor heli
        data = stlread('drone.stl');
        droneObj = trimesh(data,'FaceColor','none','EdgeColor','k');
        reducepatch(droneObj,0.02);
        droneObj.Vertices=droneObj.Vertices.*0.02; % scale
        droneObj.FaceColor='blue'; % color
        droneObj.LineWidth=0.2;
        droneObj.LineStyle=':';
        R=[cosd(-90),-sind(-90),0;...% rotate inital facing to forward
            sind(-90),cosd(-90),0;...
            0,0,1];
        tform = rigid3d(R,[0,0,0]);
        droneObj.Vertices=transformPointsForward(tform,droneObj.Vertices); 
        droneShape=droneObj.Vertices; % save shape
        
    % frame label
        frame_label = text(0,0,20, '');
        xlabel('x [m]')
        ylabel('y [m]')
        
axis([-100 100 -100 100 -100 100])

% animation loop
drawnow
pause(1);
for i = 1:length(tHist)
    tic
    % draw boat
	DrawStl(xHist_SV(1:6,i),boatObj,boatShape);
    % draw sub
    DrawStl(xHist_UV(1:6,i),subObj,subShape);
    % draw drone
    DrawStl(xHist_MH(1:6,i),droneObj,droneShape);
    
    frame_label.String="T: "+num2str(tHist(i))+" [s]";
    
    % pause if the processing time is under step time
    pause(max(dt-toc,0.0001));
end


