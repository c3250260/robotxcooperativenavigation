function U = MHcontrolMPC(t1,x1,u0,U,param)

m = size(u0,1);


if isempty(U)
    U = [u0;zeros(m*(param.nHorizon-1),1)];
else
    % warm start
    U = [U(m+1:end);zeros(m,1)];
end

err = @(U) MHerrorMPC(t1,x1,u0,U,param);
options = optimoptions('lsqnonlin','Display','none');
U = lsqnonlin(err,U,[],[],options);


%         options = optimoptions('fmincon','Display','iter','Algorithm','sqp',...
%             'CheckGradients',false,...
%             'MaxFunctionEvaluations',1e5,...
%             'MaxIterations',1000);
% 
%         Aineq   = [];
%         bineq   = [];
%         Aeq     = [];
%         beq     = [];
% %         lb      = -0.5*ones(m*N,1);
% %         ub      = 0.5*ones(m*N,1);
% %         nonlcon = @(U) nonlconMPC(t1,x1,u0,U,param);
%         U       = fmincon(err,U,Aineq,bineq,Aeq,beq,options);