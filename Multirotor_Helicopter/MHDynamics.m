function dx = MHDynamics(t, x, u, param)
%VEHICLEDYNAMICS Summary of this function goes here
%   Detailed explanation goes here

% states:
% x(1)=N
% x(2)=E
% x(3)=D
% x(4)=phi(roll)
% x(5)=theta(pitch)
% x(6)=psi(yaw) 

% x(7)=dx
% x(8)=dy
% x(9)=dz 
% x(10)=dphi 
% x(11)=dtheta
% x(12)=dpsi


% %% extract states
%     eta = x(1:6);
%     nu = x(7:12);
%     
% %% unpack params
%     m=param.m;
%     IBb=param.IBb;
%     rCBb=param.rCBb;
%     g=param.g;
%     rU1Bb=param.rU1Bb;
%     rU2Bb=param.rU2Bb;
%     rU3Bb=param.rU3Bb;
%     rU4Bb=param.rU4Bb;
%     
% %% Kinematic Transformation matrix
%     Rx=[1,0,0;...
%         0,cos(eta(4)),-sin(eta(4));...
%         0,sin(eta(4)),cos(eta(4))];
%     Ry=[cos(eta(5)),0,sin(eta(5));...
%         0,1,0;...
%         -sin(eta(5)),0,cos(eta(5))];
%     Rz=[cos(eta(6)),-sin(eta(6)),0;...
%         sin(eta(6)),cos(eta(6)),0;...
%         0,0,1];
% 
%     Rnb=Rz*Ry*Rx;
% 
%     T=[1,sin(eta(4))*tan(eta(5)),cos(eta(4))*tan(eta(5));...
%         0,cos(eta(4)),-sin(eta(4));...
%         0,sin(eta(4))/cos(eta(5)),cos(eta(4))/cos(eta(5))];
%     
%     J=[Rnb,zeros(3);...
%         zeros(3),T];
% 
% %% rigid body mass matrix
%     M = [m*eye(3),-m*skew(rCBb);...
%         m*skew(rCBb),IBb];
% 
% 
% %% rigid body coriolis matrix
%     omegaBNb = nu(4:6);
%     C = [m*skew(omegaBNb),-m*skew(omegaBNb)*skew(rCBb);...
%         m*skew(rCBb)*skew(omegaBNb), -skew(IBb*omegaBNb)];
% 
% %% Damping matrix
%     D=diag([param.d1,param.d2,param.d3,param.d4,param.d5,param.d6]);
% 
% 
% %% Forces
%     % gravitation 
%     fb=Rnb.'*[0;0;m*g];
%     mbB=[0;0;0];
%     fg=[fb;mbB];
%     
%     % actuation
%     Ba=[0,0,0,0;...% propeller thurst configuration (4 props)
%         0,0,0,0;...
%         1,1,1,1;...
%         rU1Bb(1),rU2Bb(1),rU3Bb(1),rU4Bb(1);...
%         rU1Bb(2),rU2Bb(2),rU3Bb(2),rU4Bb(2);...
%         -param.Tp,param.Tp,param.Tp,-param.Tp];... 
%     tau=Ba*u;
% 
% 
% dnu = M\(-C*nu-D*nu-fg+tau);
% dx = [nu;dnu];
% % deta = J*nu;


%% optimized form
 dx=[                                                                                                   
                                                                                                    x(7);
                                                                                                    x(8);
                                                                                                    x(9);
                                                                                                   x(10);
                                                                                                   x(11);
                                                                                                   x(12);
                                                  (981*sin(x(5)))/100 - x(7)/3 + x(8)*x(12) - x(9)*x(11);
                                        x(9)*x(10) - (981*cos(x(5))*sin(x(4)))/100 - x(7)*x(12) - x(8)/3;
u(1)/30 + u(2)/30 + u(3)/30 + u(4)/30 - x(9)/3 - (981*cos(x(4))*cos(x(5)))/100 + x(7)*x(11) - x(8)*x(10);
                                                        u(2)/40 - u(1)/40 - u(3)/40 + u(4)/40 - x(10)/20;
                                                        u(1)/40 + u(2)/40 - u(3)/40 - u(4)/40 - x(11)/20;
                                                        u(2)/20 - u(1)/20 + u(3)/20 - u(4)/20 - x(12)/20
];

