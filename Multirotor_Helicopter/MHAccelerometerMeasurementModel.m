function [h,SR] = MHAccelerometerMeasurementModel(x,u,param)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if length(x)>13
    %% monolithic state measurement model (MH: x14-26,u3-6)
    % unpack params
    sigmar = param.MH.IMU.Accel_sigmar;
    rMBb = param.MH.IMU.rMBb; 
	g = param.MH.g;
    Rbm=param.MH.IMU.Rbm;
    
    % extract states
	eta = x(13+1:13+6);
    vBNb=x(13+7:13+9);
    omegaBNb=x(13+10:13+12);
    
	% find acceleration at point
    dx = MHDynamics(0, x(13+1:13+12), u(3:6), param.MH);
    
	dvBNb=dx(7:9);
    domegaBNb=dx(10:12);
    
	% equation from week 4 tuesday slide 73
	aMNb = [eye(3) -skew(rMBb)]*[dvBNb;domegaBNb] + skew(omegaBNb)*(skew(omegaBNb)*rMBb + vBNb);


	% accel from gravity
	gn = [0;0;g];
        Rx=[1,0,0;...
                0,cos(eta(4)),-sin(eta(4));...
                0,sin(eta(4)),cos(eta(4))];
        Ry=[cos(eta(5)),0,sin(eta(5));...
                0,1,0;...
                -sin(eta(5)),0,cos(eta(5))];
        Rz=[cos(eta(6)),-sin(eta(6)),0;...
                sin(eta(6)),cos(eta(6)),0;...
                0,0,1];
        Rnb=Rz*Ry*Rx;

    h = Rbm.'*(aMNb-Rnb.'*gn);
    SR = sigmar*eye(3);
else
    %% individual sensor model
    % unpack params
    sigmar = param.IMU.Accel_sigmar;
    rMBb = param.IMU.rMBb;
    g = param.g;
    Rbm=param.IMU.Rbm;

    % extract states
    eta = x(1:6);
    vBNb=x(7:9);
    omegaBNb=x(10:12);


    % find acceleration at point
    dx = MHDynamics(0, x, u, param);


    dvBNb=dx(7:9);
    domegaBNb=dx(10:12);

        % equation from week 4 tuesday slide 73
            aMNb = [eye(3) -skew(rMBb)]*[dvBNb;domegaBNb] + skew(omegaBNb)*(skew(omegaBNb)*rMBb + vBNb);


        % accel from gravity
            gn = [0;0;g];
            Rx=[1,0,0;...
                0,cos(eta(4)),-sin(eta(4));...
                0,sin(eta(4)),cos(eta(4))];
            Ry=[cos(eta(5)),0,sin(eta(5));...
                0,1,0;...
                -sin(eta(5)),0,cos(eta(5))];
            Rz=[cos(eta(6)),-sin(eta(6)),0;...
                sin(eta(6)),cos(eta(6)),0;...
                0,0,1];
            Rnb=Rz*Ry*Rx;

    h = Rbm.'*(aMNb-Rnb.'*gn);
    SR = sigmar*eye(3);
end

