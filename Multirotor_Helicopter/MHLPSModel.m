function [h,SR] = MHLPSModel(x,u,param)

%% monolithic state measurement model (SV: x1-13,u1-2) (MH: x14-26,u3-6)
% location of the boat
rBNn = x(1:3);

sigmar = param.MH.LPS.sigmar;                 % Range beacon stdev [m]
SR = 0;
assert(isequal(SR,triu(SR)),'Expect SR to be upper triangular');

rMBb = [0;0;0];             % Position of LPS w.r.t B expressed in {b}
 

% Extract states from quadcopter
rHNn = x(13+1:13+3);
omega = x(13+4:13+6);

    Rx=[1,0,0;...
        0,cos(omega(1)),-sin(omega(1));...
        0,sin(omega(1)),cos(omega(1))];
    Ry=[cos(omega(2)),0,sin(omega(2));...
        0,1,0;...
        -sin(omega(2)),0,cos(omega(2))];
    Rz=[cos(omega(3)),-sin(omega(3)),0;...
        sin(omega(3)),cos(omega(3)),0;...
        0,0,1];
    Rnb=Rz*Ry*Rx;  
    Rbn=Rnb.';
rMNb = Rbn*(rBNn-rHNn)+rMBb;


%distance between rBNn(boat) and quadcopter
h = norm(rMNb);
SR =sigmar; 
