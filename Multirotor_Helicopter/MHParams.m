function param = MHParams()
% params for a surface vessel

%% vehicle params       
    % dimension 
        param.l=2;
        param.w=2;
        
    % mass
        param.m=30;
        param.rCBb=[0;0;0]; %location of COM
        
    % moments of inertia
        param.Ix=20;
        param.Iy=20;
        param.Iz=20;
        param.IBb  	= diag([param.Ix,param.Iy,param.Iz]);
    
    % damping
        param.d1=10;
        param.d2=10;
        param.d3=10;
        param.d4=1;
        param.d5=1;
        param.d6=1;
        
    % actuation (4 props)
        param.rU1Bb=[-0.5;0.5;0];
        param.rU2Bb=[0.5;0.5;0];
        param.rU3Bb=[-0.5;-0.5;0];
        param.rU4Bb=[0.5;-0.5;0];
        param.Tp=1; % props torque(yaw) constant

    % control
        param.nHorizon=10;% Control horizon
        
     % sensors
        % IMU
            param.IMU.rMBb=[0;0;0];         % Position of IMU w.r.t B expressed in {b}
            param.IMU.Rbm=eye(3);        	% Rbm = [m1b, m2b, m3b], where mib is the ith accelerometer axis expressed in {b}
            % Accelerometer
                param.IMU.Accel_sigmar=0.2;     % Accelerometer channel stdev [m/s^2]
            % Gyro
                param.IMU.Gyro_sigmar=2*pi/180;	% Gyroscope channel stdev [rad/s]
            % Magnetometer
                param.IMU.Mag_sigmar=1;         % Magnetometer channel stdev [m/s^2]
        % Bearing
            param.Bearing.sigmar = 1*pi/180;
        % LPS
            param.LPS.sigmar = 0.1;             % Range beacon stdev [m]
        % GPS
            param.GPS.sigmar=6;
            
            
%% enviroment params
    param.g=9.81; 
	% local reference coord for GPS
    param.Latitude = -32.959008; 
    param.Longitude = 151.777433;
end

