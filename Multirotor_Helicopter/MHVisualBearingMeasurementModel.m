function [h,SR] = MHVisualBearingMeasurementModel(x,u,param)
%SVVISUALBEARINGMEASUREMENTMODEL Summary of this function goes here
%   Detailed explanation goes here

% sigmar=param.Bearing.sigmar;
% 
% % beacon location and count
% rPNn_all=BeaconLoc();
% temp=size(rPNn_all);
% beaconCount=temp(2);
% 
% % position of the measurement
% rMBb = [0;0;0];
% 
% % position of the heli
% rHNn = x(1:3);
% omega = x(4:6);
% 
% % transform relative position to {b}
%     Rx=[1,0,0;...
%         0,cos(omega(1)),-sin(omega(1));...
%         0,sin(omega(1)),cos(omega(1))];
%     Ry=[cos(omega(2)),0,sin(omega(2));...
%         0,1,0;...
%         -sin(omega(2)),0,cos(omega(2))];
%     Rz=[cos(omega(3)),-sin(omega(3)),0;...
%         sin(omega(3)),cos(omega(3)),0;...
%         0,0,1];
%     Rnb=Rz*Ry*Rx;  
%     Rbn=Rnb.';
% rPNb_all = Rbn*(rPNn_all-rHNn)+rMBb;
% 
% 
% h=zeros(beaconCount*3,1);
% h(1:3)=rPNb_all(1:3,1)/norm(rPNb_all(1:3,1));
% h(4:6)=rPNb_all(1:3,1)/norm(rPNb_all(1:3,2));
% h(7:9)=rPNb_all(1:3,1)/norm(rPNb_all(1:3,3));
% h(10:12)=rPNb_all(1:3,1)/norm(rPNb_all(1:3,4));
% SR =sigmar*eye(beaconCount*3);

%% monolithic state measurement model (SV: x1-13,u1-2) (MH: x14-26,u3-6)
sigmar = param.MH.Bearing.sigmar;                 % Range beacon stdev [m]
rMBb = [0;0;0];

% location of the boat
rBNn = x(1:3);

% location of the quadcopter
rHNn = x(13+1:13+3);
omega = x(13+4:13+6);

    Rx=[1,0,0;...
        0,cos(omega(1)),-sin(omega(1));...
        0,sin(omega(1)),cos(omega(1))];
    Ry=[cos(omega(2)),0,sin(omega(2));...
        0,1,0;...
        -sin(omega(2)),0,cos(omega(2))];
    Rz=[cos(omega(3)),-sin(omega(3)),0;...
        sin(omega(3)),cos(omega(3)),0;...
        0,0,1];
    Rnb=Rz*Ry*Rx;  
    Rbn=Rnb.';
	rMNb = Rbn*(rBNn-rHNn)+rMBb;
 
 % returns normalized vector from quadcopter to boat
 h=rMNb/norm(rMNb);
 SR =sigmar*eye(3);
