function e = MHerrorMPC(t1, x1, u0, U, param) %#ok<INUSL>

dt = 0.05;
N = param.nHorizon; % Control horizon

% Penalty coefficients
sqrtx      = sqrt(3000);
sqrtqpsi    = sqrt(40000);
sqrtru      = sqrt(0.2);

e = zeros(10*N,1);

t = t1;
x = x1;

nu = length(u0);

for i = 1:N                 % for each step in the control horizon
    u = U(nu*i-nu+1:nu*i);
    
    % Do one step of RK4 integration
    f1 = MHControlDynamics(t,        x,           u, param);
    f2 = MHControlDynamics(t + dt/2, x + f1*dt/2, u, param);
    f3 = MHControlDynamics(t + dt/2, x + f2*dt/2, u, param);
    f4 = MHControlDynamics(t + dt,   x + f3*dt,   u, param);
    x = x + (f1 + 2*f2 + 2*f3 + f4)*dt/6;
    t = t + dt;
    
    % Evaluate trajectory
    X=MHTrajectoryGen(t);
    rPNn = X(1:3);
    psistar = X(4:6);
    
    % Extract states
    rBNn = x(1:3);         % Displacement states
    pnb= x(4:6);           % rotation
   
    % Error for current step
    e((i-1)*10+1:i*10) = [sqrtx*(rPNn-rBNn);...
                        sqrtqpsi*(psistar-pnb);...
                        sqrtru*(u)];
end