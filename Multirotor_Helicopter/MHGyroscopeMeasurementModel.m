function [h,SR] = MHGyroscopeMeasurementModel(x,u,param)


if length(x)>13
    %% monolithic state measurement model (MH: x14-26,u3-6)
	% Unpack params
    sigmar = param.MH.IMU.Gyro_sigmar;
    Rbm=param.MH.IMU.Rbm;
    
	% Extract states
    omegaBNb=x(13+10:13+12);
    b=x(13+13); % gyro bias state augmentation
    
	h = Rbm.'*omegaBNb + b;
    SR = sigmar*eye(3);
    
else
    %% individual sensor model
    % Unpack params
    sigmar = param.IMU.Gyro_sigmar;
    Rbm=param.IMU.Rbm;

    % Extract states
    omegaBNb=x(10:12);
    b=x(13); % gyro bias state augmentation


    h = Rbm.'*omegaBNb + b;
    SR = sigmar*eye(3);
end