function [h,SR] = MHGPSMeasurementModel(x,u,param)

if length(x)>13
    %% monolithic state measurement model (MH: x14-26,u3-6)
	% Unpack params
    sigmar = param.MH.GPS.sigmar;
	% reference point
    Lat=-90+param.MH.Latitude* pi / 180; 
    Long=param.MH.Longitude* pi / 180;
    RefHeight = 0; % reference coord height relative to average water level
    
	% Extract states
    rBNn=x(13+1:13+3);
    
	% find ECEF location
        Rx=[1,0,0;...
            0,cos(Long),-sin(Long);...
            0,sin(Long),cos(Long)];
        Ry=[cos(Lat),0,sin(Lat);...
            0,1,0;...
            -sin(Lat),0,cos(Lat)];
    Ren=Ry*Rx;
    
	EarthRad = 6378137;
    e = 0.08181919;
    Rn = EarthRad / (1 - e^2 * (sin(Lat))^2)^0.5; % Earth Normal Radius

    % Compute point in Geocentric coordinates
    rNOe = [(Rn+RefHeight)*cos(Lat)*cos(Long);
            (Rn+RefHeight)*cos(Lat)*sin(Long);
            (Rn*(1-e^2) + RefHeight)*sin(Lat)];

    h = Ren.'*rBNn + rNOe;
    SR = sigmar*eye(3);
    
else
	%% individual sensor model
    % Unpack params
    sigmar = param.GPS.sigmar;
    % reference point
    Lat=-90+param.Latitude* pi / 180; 
    Long=param.Longitude* pi / 180;
    RefHeight = 0; % reference coord height relative to average water level

    % Extract states
    rBNn=x(1:3);

    % find ECEF location
        Rx=[1,0,0;...
            0,cos(Long),-sin(Long);...
            0,sin(Long),cos(Long)];
        Ry=[cos(Lat),0,sin(Lat);...
            0,1,0;...
            -sin(Lat),0,cos(Lat)];
    Ren=Ry*Rx;

    EarthRad = 6378137;
    e = 0.08181919;
    Rn = EarthRad / (1 - e^2 * (sin(Lat))^2)^0.5; % Earth Normal Radius

    % Compute point in Geocentric coordinates
    rNOe = [(Rn+RefHeight)*cos(Lat)*cos(Long);
            (Rn+RefHeight)*cos(Lat)*sin(Long);
            (Rn*(1-e^2) + RefHeight)*sin(Lat)];


    h = Ren.'*rBNn + rNOe;
    SR = sigmar*eye(3);
end
