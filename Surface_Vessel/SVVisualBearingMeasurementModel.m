function [h,SR] = SVVisualBearingMeasurementModel(x,u,param)
%SVVISUALBEARINGMEASUREMENTMODEL Summary of this function goes here
%   Detailed explanation goes here

if length(x)>13
    %% monolithic state measurement model (SV: x1-13,u1-2)
    sigmar=param.SV.Bearing.sigmar;
    
    % beacon location and count
    rPNn_all=BeaconLoc();
    temp=size(rPNn_all);
    beaconCount=temp(2);
    
    rMBb = [0;0;0];
    
    % transform to {b}
        Rx=[1,0,0;...
            0,cos(x(4)),-sin(x(4));...
            0,sin(x(4)),cos(x(4))];
        Ry=[cos(x(5)),0,sin(x(5));...
            0,1,0;...
            -sin(x(5)),0,cos(x(5))];
        Rz=[cos(x(6)),-sin(x(6)),0;...
            sin(x(6)),cos(x(6)),0;...
            0,0,1];
        Rnb=Rz*Ry*Rx;  
        Rbn=Rnb.';
    rMNn = Rnb*rMBb+x(1:3);
    
    rPNb_all=Rbn*rPNn_all;
    
    
    h = atan2(rMNn(2)-rPNb_all(2,:),rMNn(1)-rPNb_all(1,:)).';
    SR =sigmar*eye(beaconCount);
    
else
    %% individual sensor model
    sigmar=param.Bearing.sigmar;

    % beacon location and count
    rPNn_all=BeaconLoc();
    temp=size(rPNn_all);
    beaconCount=temp(2);

    rMBb = [0;0;0];

    % transform to {b}
        Rx=[1,0,0;...
            0,cos(x(4)),-sin(x(4));...
            0,sin(x(4)),cos(x(4))];
        Ry=[cos(x(5)),0,sin(x(5));...
            0,1,0;...
            -sin(x(5)),0,cos(x(5))];
        Rz=[cos(x(6)),-sin(x(6)),0;...
            sin(x(6)),cos(x(6)),0;...
            0,0,1];
        Rnb=Rz*Ry*Rx;  
        Rbn=Rnb.';
    rMNn = Rnb*rMBb+x(1:3);

    rPNb_all=Rbn*rPNn_all;


    h = atan2(rMNn(2)-rPNb_all(2,:),rMNn(1)-rPNb_all(1,:)).';
    SR =sigmar*eye(beaconCount);
end

