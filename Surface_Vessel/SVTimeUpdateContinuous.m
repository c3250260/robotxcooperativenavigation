function [mu,S] = SVTimeUpdateContinuous(mu,S,u,processModel,param,timestep,integrationMethod,transformMethod,makeStateFeasible)

%
% Integrate a continuous-time process over one time step
%

%
% Input arguments
%
% mu: state mean at time index k-1
% S:  upper triangular matrix such that S.'*S is the covariance of the state at time index k-1
% u:  input (ZOH over the time step)
% processModel:         function handle to continuous-time process model
% param:                parameters to pass to process model
% timestep:             Time step for integration
% integrationMethod:    'Euler' or 'RK4' integration method
% transformMethod:      'affine' or 'unscented' transform
% makeStateFeasible: 	function handle to enforce state constraints (unscented transform only, optional)
%
% Output arguments
%
% mu: state mean at time index k
% S:  upper triangular matrix such that S.'*S is the covariance of the state at time index k
%

switch transformMethod
    case 'affine'
        transform = @affineTransform;
    case 'unscented'
        transform = @unscentedTransform;
    otherwise
        error('Expecting transformMethod to be ''affine'' or ''unscented''');
end

if nargin < 9
    makeStateFeasible = @(x,param) x;
end
makeFeasibleFunc = @(x) makeStateFeasible(x,param);

% Perform one step of integration for the SDE
%   dx = f(x)*dt + dw, where dw ~ (0, Q*dt)
% to map (mu[k],P[k]) |---> (mu[k+1],P[k+1])
switch integrationMethod
    case 'Euler'
        % Since Euler SDE integration remains affine in the noise increment, we
        % can build a discrete-time model directly from the continuous-time model
        % using an adapter function.
        processDiscreteFunc = @(x) EulerSDEAdapter(processModel,x,u,param,timestep);
        [mu, S] = transform(mu,S,processDiscreteFunc,makeFeasibleFunc);
    case 'RK4'
        % Since the RK4 SDE integration is not affine in the noise increment, we
        % augment the state with the noise increment and transform this jointly
        % through the RK4 method.
        RK4Func = @(xdw) RK4SDEHelper(processModel,xdw,u,param,timestep);
        [~,SQ] = SVProcessModel(mu,u,param);
        muxdw = [mu; zeros(size(mu))];
        Sxdw = blkdiag(S, SQ*realsqrt(timestep));
        [mu, S] = transform(muxdw,Sxdw,RK4Func,makeFeasibleFunc);
    otherwise
        error('Expecting integrationMethod to be ''Euler'' or ''RK4''');
end

% Enforce mean feasibility
mu = makeFeasibleFunc(mu);

end



