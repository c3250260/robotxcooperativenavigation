function [h,SR] = SVGyroscopeMeasurementModel(x,u,param)


if length(x)>13
    %% monolithic state measurement model (SV: x1-13,u1-2)
	% Unpack params
    sigmar = param.SV.IMU.Gyro_sigmar;
    Rbm=param.SV.IMU.Rbm;
    
	% Extract states
    omegaBNb=x(10:12);
    b=x(13); % gyro bias state augmentation
    sigmar = param.SV.IMU.Gyro_sigmar;
    
	h = Rbm.'*omegaBNb + b;
    SR = sigmar*eye(3);
    
else
    %% individual sensor model
    % Unpack params
    sigmar = param.IMU.Gyro_sigmar;
    Rbm=param.IMU.Rbm;

    % Extract states
    omegaBNb=x(10:12);
    b=x(13); % gyro bias state augmentation
    sigmar = param.IMU.Gyro_sigmar;


    h = Rbm.'*omegaBNb + b;
    SR = sigmar*eye(3);
end
