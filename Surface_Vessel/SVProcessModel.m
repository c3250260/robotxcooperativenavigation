function [f,SQ,A] = SVProcessModel(x,u,param)

if nargout > 2
    % Evaluate f(x,u) and its gradient A = df/dx
    [f,A] = SVDynamics(0,x,u,param);
else
    % Evaluate f(x,u)
    f = SVDynamics(0,x,u,param);
end

% Augment gyro bias dynamics: bdot = 0
f = [f; 0];
if nargout > 2
    A = blkdiag(A,0);
end

sigmau = 0.01;                 % Additive input torque noise intensity [N.m.s^0.5]
sigmab = 0.01*pi/180;                 % Gyro bias drift intensity [rad.s^-1.5]

% Q
Q=diag([sqrt(2)*sigmau;1e-1;1e-1;1e-1;1e-1;1e-1]);

SQ=[chol(Q(1:6,1:6)),zeros(6,6);...
    zeros(6,12)];     
% use chol

SQ = blkdiag(SQ, sigmab);   % Augment with gyro bias drift intensity

assert(isequal(SQ,triu(SQ)),'Expect SQ to be upper triangular');