function [h,SR] = SVMagnetometerModel(x,u,param)
%SURFACEMAGNETOMODEL Summary of this function goes here
%   Detailed explanation goes here

%% unpack params
sigmar = param.IMU.Accel_sigmar;
Rbm=param.IMU.Rbm;
b=0.1; %bias

%% extract states
eta = x(1:6);

 
h=Rbm*eta(4:6)*+b;
SR=eye(3)*sigmar;

end

