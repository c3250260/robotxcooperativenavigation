function dx = SVDynamics(t, x, u, param)
%VEHICLEDYNAMICS Summary of this function goes here
%   Detailed explanation goes here

% states:
% x(1)=N
% x(2)=E
% x(3)=D //unused
% x(4)=phi(roll) //unused
% x(5)=theta(pitch)
% x(6)=psi(yaw) 

% x(7)=dx
% x(8)=dy
% x(9)=dz //unused
% x(10)=dphi //unused
% x(11)=dtheta
% x(12)=dpsi


% %% extract states
%     % reduced D.O.F
%     x(3)=-0.1;
%     x(4)=0;
%     x(9)=0;
%     x(10)=0;
%     
%     eta = x(1:6);
%     nu = x(7:12);
%     
% %% unpack params
%     m=param.m;
%     IBb=param.IBb;
%     rCBb=param.rCBb;
%     g=param.g;
% 
% 
% %% Kinematic Transformation matrix
%     Rx=[1,0,0;...
%         0,cos(eta(4)),-sin(eta(4));...
%         0,sin(eta(4)),cos(eta(4))];
%     Ry=[cos(eta(5)),0,sin(eta(5));...
%         0,1,0;...
%         -sin(eta(5)),0,cos(eta(5))];
%     Rz=[cos(eta(6)),-sin(eta(6)),0;...
%         sin(eta(6)),cos(eta(6)),0;...
%         0,0,1];
% 
%     Rnb=Rz*Ry*Rx;
% 
%     T=[1,sin(eta(4))*tan(eta(5)),cos(eta(4))*tan(eta(5));...
%         0,cos(eta(4)),-sin(eta(4));...
%         0,sin(eta(4))/cos(eta(5)),cos(eta(4))/cos(eta(5))];
%     
%     J=[Rnb,zeros(3);...
%         zeros(3),T];
% 
% %% rigid body mass matrix
%     M = [m*eye(3),-m*skew(rCBb);...
%         m*skew(rCBb),IBb];
% 
% 
% %% rigid body coriolis matrix
% 
%     omegaBNb = nu(4:6);
%     C = [m*skew(omegaBNb),-m*skew(omegaBNb)*skew(rCBb);...
%         m*skew(rCBb)*skew(omegaBNb), -skew(IBb*omegaBNb)];
% 
% %% Damping matrix
%     D=diag([param.d1,param.d2,0,0,param.d5,param.d6]);
% 
% 
% %% Forces
%     % gravitation (simplified model, none)
%         
%     % actuation
%     Ba=[1,1;... % propeller thurst configuration
%         0,0;...
%         0,0;...
%         0,0;...
%         0,0;...
%         param.rU1Bb(2),param.rU2Bb(2)];
%     tau=Ba*u;
% 
% 
% dnu = M\(-C*nu-D*abs(J*nu).*(J*nu)+tau);
% dx = [nu;dnu];
% % deta = J*nu;


%% optimized form
dx=[
                                                                                                                                                                              x(7);
                                                                                                                                                                              x(8);
                                                                                                                                                                                 0;
                                                                                                                                                                                 0;
                                                                                                                                                                             x(11);
                                                                                                                                                                             x(12);
u(1)/400 + u(2)/400 + x(8)*x(12) + (abs(x(8)*sin(x(6)) - x(7)*cos(x(5))*cos(x(6)))*x(8)*sin(x(6)))/4 - (abs(x(8)*sin(x(6)) - x(7)*cos(x(5))*cos(x(6)))*x(7)*cos(x(5))*cos(x(6)))/4;
                - x(7)*x(12) - (5*abs(x(8)*cos(x(6)) + x(7)*cos(x(5))*sin(x(6)))*x(8)*cos(x(6)))/2 - (5*abs(x(8)*cos(x(6)) + x(7)*cos(x(5))*sin(x(6)))*x(7)*cos(x(5))*sin(x(6)))/2;
                                                                                                                                                                        x(7)*x(11);
                                                                                                                                                                                 0;
                                                                                                                                                        -(3*x(11)*abs(x(11)))/2000;
                                                                                          (3*u(1))/200000 - (3*u(2))/200000 - (3*x(12)*abs(x(12)))/(2000*abs(cos(x(5)))*cos(x(5)));
 
];

