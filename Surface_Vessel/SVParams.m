function param = SVParams()
% params for a surface vessel

%% vehicle params       
    % dimension 
        param.l=10;
        param.w=4;
        
    % mass
        param.m=400;
        param.rCBb=[0;0;0]; %location of COM
        
    % moments of inertia
        param.Ix=1e5;
        param.Iy=1e5;
        param.Iz=1e5;
        param.IBb  	= diag([param.Ix,param.Iy,param.Iz]);
    
    % damping
        param.d1=100;
        param.d2=1e3;
        param.d5=150;
        param.d6=150;
        
    % actuation
        param.rU1Bb=[-4;1.5;-0.3];
        param.rU2Bb=[-4;-1.5;-0.3];
        
     % sensors
        % IMU
            param.IMU.rMBb=[0;0;0];         % Position of IMU w.r.t B expressed in {b}
            param.IMU.Rbm=eye(3);        	% Rbm = [m1b, m2b, m3b], where mib is the ith accelerometer axis expressed in {b}
            % Accelerometer
                param.IMU.Accel_sigmar=0.2;     % Accelerometer channel stdev [m/s^2]
            % Gyro
                param.IMU.Gyro_sigmar=2*pi/180;	% Gyroscope channel stdev [rad/s]
            % Magnetometer
                param.IMU.Mag_sigmar=5;         % Magnetometer channel stdev [m/s^2]
        % Bearing
            param.Bearing.sigmar = 2*pi/180;
        % GPS
            param.GPS.sigmar=6;

                
     
%% enviroment params
    param.g = 9.81; 
    % local reference coord for GPS (somewhere near newcastle)
    param.Latitude = -32.959008; 
    param.Longitude = 151.777433;
    
end

