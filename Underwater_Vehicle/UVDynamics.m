function dx = UVDynamics(t, x, u, param)
%VEHICLEDYNAMICS Summary of this function goes here
%   Detailed explanation goes here

% states:
% x(1)=N
% x(2)=E
% x(3)=D
% x(4)=phi(roll)
% x(5)=theta(pitch)
% x(6)=psi(yaw) 

% x(7)=dx
% x(8)=dy
% x(9)=dz 
% x(10)=dphi 
% x(11)=dtheta
% x(12)=dpsi


% %% extract states
%     eta = x(1:6);
%     nu = x(7:12);
%     
% %% unpack params
%     m=param.m;
%     IBb=param.IBb;
%     rCBb=param.rCBb;
%     g=param.g;
% 
% 
% %% Kinematic Transformation matrix
%     Rx=[1,0,0;...
%         0,cos(eta(4)),-sin(eta(4));...
%         0,sin(eta(4)),cos(eta(4))];
%     Ry=[cos(eta(5)),0,sin(eta(5));...
%         0,1,0;...
%         -sin(eta(5)),0,cos(eta(5))];
%     Rz=[cos(eta(6)),-sin(eta(6)),0;...
%         sin(eta(6)),cos(eta(6)),0;...
%         0,0,1];
% 
%     Rnb=Rz*Ry*Rx;
% 
%     T=[1,sin(eta(4))*tan(eta(5)),cos(eta(4))*tan(eta(5));...
%         0,cos(eta(4)),-sin(eta(4));...
%         0,sin(eta(4))/cos(eta(5)),cos(eta(4))/cos(eta(5))];
%     
%     J=[Rnb,zeros(3);...
%         zeros(3),T];
% 
% %% rigid body mass matrix
%     M = [m*eye(3),-m*skew(rCBb);...
%         m*skew(rCBb),IBb];
% 
% 
% %% rigid body coriolis matrix
%     omegaBNb = nu(4:6);
%     C = [m*skew(omegaBNb),-m*skew(omegaBNb)*skew(rCBb);...
%         m*skew(rCBb)*skew(omegaBNb), -skew(IBb*omegaBNb)];
% 
% %% Damping matrix
%     D=diag([param.d1,param.d2,param.d3,param.d4,param.d5,param.d6]);
% 
% 
% %% Forces
%     % gravitation (assume neutrally buoyant)
%         
%     % actuation
%     Ba=eye(6);... % propeller thurst configuration (simplified, fully actuated)
%     tau=Ba*u;
% 
% 
% dnu = M\(-C*nu-D*(abs(nu).*nu)+tau);
% dx = [nu;dnu];
% % deta = J*nu;


%% optimized form
dx=[
                                                     x(7);
                                                     x(8);
                                                     x(9);
                                                    x(10);
                                                    x(11);
                                                    x(12);
u(1)/400 + x(8)*x(12) - x(9)*x(11) - (3*x(7)*abs(x(7)))/4;
u(2)/400 - x(7)*x(12) + x(9)*x(10) - (3*x(8)*abs(x(8)))/4;
u(3)/400 + x(7)*x(11) - x(8)*x(10) - (3*x(9)*abs(x(9)))/4;
                       u(4)/10000 - (x(10)*abs(x(10)))/50;
                       u(5)/10000 - (x(11)*abs(x(11)))/50;
                       u(6)/10000 - (x(12)*abs(x(12)))/50;
 ];

