function [h,SR] = UVPingerMeasurementModel(x,u,param)

sigmar=param.UV.Pinger.sigmar;

%% monolithic state measurement model (SV: x1-13,u1-2) (UV: x27-39,u7-12)
% position of the boat
rBNn = x(1:3);

% position of the sub
rUNn = x(27:29);
omega = x(30:32);


rMBb = [0;0;0];



% transform relative position to {b}
    Rx=[1,0,0;...
        0,cos(omega(1)),-sin(omega(1));...
        0,sin(omega(1)),cos(omega(1))];
    Ry=[cos(omega(2)),0,sin(omega(2));...
        0,1,0;...
        -sin(omega(2)),0,cos(omega(2))];
    Rz=[cos(omega(3)),-sin(omega(3)),0;...
        sin(omega(3)),cos(omega(3)),0;...
        0,0,1];
    Rnb=Rz*Ry*Rx;  
    Rbn=Rnb.';
rMNb = Rbn*(rBNn-rUNn)+rMBb;


% returns normalized vector pointing at the boat
h = rMNb/norm(rMNb);
SR =sigmar*eye(3);
