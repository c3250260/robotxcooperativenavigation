function param = UVParams()
% params for a surface vessel

%% vehicle params       
    % dimension 
        param.l=4;
        param.w=4;
        
    % mass
        param.m=400;
        param.rCBb=[0;0;0]; %location of COM
        
    % moments of inertia
        param.Ix=1e4;
        param.Iy=1e4;
        param.Iz=1e4;
        param.IBb  	= diag([param.Ix,param.Iy,param.Iz]);
    
    % damping
        param.d1=300;
        param.d2=300;
        param.d3=300;
        param.d4=200;
        param.d5=200;
        param.d6=200;
        
    % actuation
        % simplified, fully actuated 
        
     % sensors
        % IMU
            param.IMU.rMBb=[0;0;0];         % Position of IMU w.r.t B expressed in {b}
            param.IMU.Rbm=eye(3);        	% Rbm = [m1b, m2b, m3b], where mib is the ith accelerometer axis expressed in {b}
            % Accelerometer
                param.IMU.Accel_sigmar=0.2;     % Accelerometer channel stdev [m/s^2]
            % Gyro
                param.IMU.Gyro_sigmar=2*pi/180;	% Gyroscope channel stdev [rad/s]
            % Magnetometer
                param.IMU.Mag_sigmar=1;         % Magnetometer channel stdev [m/s^2]
        % Pinger
            param.Pinger.sigmar=1*pi/180;
            
%% enviroment params
    param.g=9.81; 
    
    
end

