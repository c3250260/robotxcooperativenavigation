function [h,SR] = UVAccelerometerMeasurementModel(x,u,param)


if length(x)>13
    %% monolithic state measurement model (UV: x27-39,u7-12)
    % unpack params
    sigmar = param.UV.IMU.Accel_sigmar;
    rMBb = param.UV.IMU.rMBb; 
	g = param.UV.g;
    Rbm=param.UV.IMU.Rbm;
    
    % extract states
	eta = x(13*2+1:14*2+6);
    vBNb=x(13*2+7:13*2+9);
    omegaBNb=x(13*2+10:13*2+12);
    
	% find acceleration at point
    dx = UVDynamics(0, x(13*2+1:13*2+12), u(7:12), param.UV);
    
	dvBNb=dx(7:9);
    domegaBNb=dx(10:12);
    
	% equation from week 4 tuesday slide 73
	aMNb = [eye(3) -skew(rMBb)]*[dvBNb;domegaBNb] + skew(omegaBNb)*(skew(omegaBNb)*rMBb + vBNb);


	% accel from gravity
	gn = [0;0;g];
        Rx=[1,0,0;...
                0,cos(eta(4)),-sin(eta(4));...
                0,sin(eta(4)),cos(eta(4))];
        Ry=[cos(eta(5)),0,sin(eta(5));...
                0,1,0;...
                -sin(eta(5)),0,cos(eta(5))];
        Rz=[cos(eta(6)),-sin(eta(6)),0;...
                sin(eta(6)),cos(eta(6)),0;...
                0,0,1];
        Rnb=Rz*Ry*Rx;

    h = Rbm.'*(aMNb-Rnb.'*gn);
    SR = sigmar*eye(3);
else
    %% individual sensor model
    % unpack params
    sigmar = param.IMU.Accel_sigmar;
    rMBb = param.IMU.rMBb;
    g = param.g;
    Rbm=param.IMU.Rbm;

    % extract states
    eta = x(1:6);
    vBNb=x(7:9);
    omegaBNb=x(10:12);


    % find acceleration at point
    dx = UVDynamics(0, x, u, param);


    dvBNb=dx(7:9);
    domegaBNb=dx(10:12);

        % equation from week 4 tuesday slide 73
            aMNb = [eye(3) -skew(rMBb)]*[dvBNb;domegaBNb] + skew(omegaBNb)*(skew(omegaBNb)*rMBb + vBNb);


        % accel from gravity
            gn = [0;0;g];
            Rx=[1,0,0;...
                0,cos(eta(4)),-sin(eta(4));...
                0,sin(eta(4)),cos(eta(4))];
            Ry=[cos(eta(5)),0,sin(eta(5));...
                0,1,0;...
                -sin(eta(5)),0,cos(eta(5))];
            Rz=[cos(eta(6)),-sin(eta(6)),0;...
                sin(eta(6)),cos(eta(6)),0;...
                0,0,1];
            Rnb=Rz*Ry*Rx;

    h = Rbm.'*(aMNb-Rnb.'*gn);
    SR = sigmar*eye(3);

end

