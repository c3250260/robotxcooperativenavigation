function [h,SR] = UVGyroscopeMeasurementModel(x,u,param)


if length(x)>13
    %% monolithic state measurement model (UV: x27-39,u7-12)
	% Unpack params
    sigmar = param.UV.IMU.Gyro_sigmar;
    Rbm=param.UV.IMU.Rbm;
    
	% Extract states
    omegaBNb=x(13*2+10:13*2+12);
    b=x(13*3); % gyro bias state augmentation
    sigmar = param.UV.IMU.Gyro_sigmar;
    
	h = Rbm.'*omegaBNb + b;
    SR = sigmar*eye(3);
    
else
	%% individual sensor model
    % Unpack params
    sigmar = param.IMU.Gyro_sigmar;
    Rbm=param.IMU.Rbm;

    % Extract states
    omegaBNb=x(10:12);
    b=x(13); % gyro bias state augmentation

    h = Rbm.'*omegaBNb + b;
    SR = sigmar*eye(3);
end