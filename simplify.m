%% simplify equation for better computation performance
% run this after modifying dynamics

clc
clear
close all

addpath Underwater_Vehicle
addpath Surface_Vessel
addpath Multirotor_Helicopter
addpath Helper

SVparam=SVParams();
MHparam=MHParams();
UVparam=UVParams();

syms t
x=sym('x',[12 1]);


%----------------------------------
% change this part only
u=sym('u',[2 1]);
dx = SVDynamics(t, x, u, SVparam);

% u=sym('u',[6 1]);
% dx = MHDynamics(t, x, u, MHparam);

% u=sym('u',[4 1]);
% dx = UVDynamics(t, x, u, UVparam);
%----------------------------------
dx=simplify(dx,'steps',100);

dx=string(dx);
for i=12:-1:1
    dx=replace(dx,"x"+i,"x("+i+")");
end
for i=6:-1:1
    dx=replace(dx,"u"+i,"u("+i+")");
end

dx=str2sym(dx)